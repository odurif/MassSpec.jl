```@meta
DocTestSetup = quote
    using MassSpec
end
```

# Define events

Events are whatever is interesting in the data. They are defined with a `Dict`. This `Dict` needs a `String` as key and a `UnitRange{Int64}` or a `Tuple` of `UnitRange{Int64}` as value.

One way to go is to define manually the events, for example:

```jldoctest
julia> cycles=Dict("flow=1, NO=0, lights OFF" => (200:250,500:550), "flow=2, NO=0, lights ON" => (800:850));
```

Another way to go is to load events from a file, as a `JSON` file with [`json2dict`](@ref MassSpec.json2dict) function or from an `indications.txt` kind file with [`txt2dict`](@ref MassSpec.txt2dict) function.

## Ranges from rangeSetting.json file

```jldoctest rangeSetting
julia> json2dict("./src/examples/data/RangeSetting.json")
Dict{String, Tuple{UnitRange{Int64}}} with 5 entries:
  "pts=2" => (452:477,)
  "pts=3" => (854:869,)
  "pts=4" => (1053:1068,)
  "pts=1" => (153:168,)
  "pts=5" => (1291:1306,)
```

Conversely, to save a dict of ranges into a JSON file, see [`dict2json`](@ref MassSpec.dict2json).

### Group keys

```jldoctest rangeSetting
julia> json2dict("./src/examples/data/RangeSetting.json")
Dict{String, Tuple{UnitRange{Int64}}} with 5 entries:
  "pts=2" => (452:477,)
  "pts=3" => (854:869,)
  "pts=4" => (1053:1068,)
  "pts=1" => (153:168,)
  "pts=5" => (1291:1306,)
```

## Create a Dict of cycles based on indications.txt file

Events defined in a text file can be convected as `Dict`.

The text file synthax must be: 
```
NUMBER LINE; COMMENT 
```
When no commot-dot is in the line, the line is completly ignored. However, if the line must be somehow be taken into account for the cycle, but not keepted ultimately (important if cycle is shifted), it must start by `#`. 

See example: ./src/examples/data/indications.txt
```
This line and the two bellow are not taken into account while converting this file as a dict
NOx air background: NO=1.5ppb, NO2=7.5ppb
NOx with injection: NO=38ppb, NO2=6ppb
167; pts=1
451:466; pts=2
868; pts=3
1067; pts=4
1305; pts=5
#1405; this pts will be ignored
```

See [`txt2dict`](@ref).

```jldoctest txt2dict
julia> txt2dict("./src/examples/data/indications.txt"; pts=15, margin=5, shifted=false)
Dict{String, Tuple{Vararg{UnitRange{Int64}}}} with 5 entries:
  "pts=2" => (446:461,)
  "pts=3" => (848:863,)
  "pts=4" => (1047:1062,)
  "pts=1" => (147:162,)
  "pts=5" => (1285:1300,)
```
