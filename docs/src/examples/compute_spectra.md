```@meta
DocTestSetup = quote
    using MassSpec, Plots
    traces = LoadTraceFile("./src/examples/data/traces.h5")
    rawData = LoadRawData("./src/examples/data/data.h5");
end
```

# [Spectrum](@id spectrum_example)

Spectrum data have to be `DataFrame` type containing two columns: `mass`, `signal`.

They can either be loaded from a simple ASCII file using [`LoadSpectrumFile`](@ref) or directly extracted from the raw data ([see below Compute spectra](#Compute spectra)).

## [Compute spectra](@id computeSpectrum_example)

Spectra can be computed from raw data and saved in a plain text file using [`computeSpectrum`](@ref) .

```jldoctest computeSpectrum
julia> computeSpectrum(rawData; range=152:167, save=:none, progress=false)
430001×2 DataFrame
    Row │ mass        signal
        │ Float32     Float64
────────┼────────────────────────
      1 │   0.165192  0.0
      2 │   0.165228  0.0
      3 │   0.165265  0.0
      4 │   0.165301  0.0340968
      5 │   0.165338  0.00877241
      6 │   0.165374  0.0
      7 │   0.165411  0.0
      8 │   0.165447  0.0
   ⋮    │     ⋮           ⋮
 429995 │ 388.008     0.0
 429996 │ 388.009     0.0
 429997 │ 388.011     0.0
 429998 │ 388.013     0.0
 429999 │ 388.015     0.0
 430000 │ 388.016     0.0
 430001 │ 388.018     0.0
              429986 rows omitted
```

### [Calibrate and average spectrum](@id calibration_computeSpectrum_example)

Calibration can be performed with 2 or 3 parameters. See [`calibration`](@ref howto_mass_calibration) for details.

```@example computeSpectrum
using MassSpec # hide
rawData = LoadRawData("./data/data.h5"); # hide

m1=masses("NH3+")["m/z"]
m2=masses("C5H8O2H+")["m/z"]
m3=masses("C6H4I2H+")["m/z"]

computeSpectrum(rawData; mass_calibration=[m1,m2,m3], save=:auto, progress=false);

nothing # hide
```

Note: if `save=:auto` then the averaged spectrum is save into the raw data. This parameter can also be a `string`, to save directly the output into a plain file.
