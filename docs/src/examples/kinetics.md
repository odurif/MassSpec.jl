```@meta
DocTestSetup = quote
    using MassSpec
    traces = LoadTraceFile("./src/examples/data/traces.h5")
    raw = LoadRawData("./src/examples/data/data.h5")
end
```

# Tools

## [Mean Range](@id meanTraces_example)

```@example meanTraces
using MassSpec, Plots

traces = LoadTraceFile("./data/traces.h5")

ranges=Dict{String,UnitRange{Int64}}()
ranges["pts=1"]=152:167
ranges["pts=2"]=451:466
ranges["pts=3"]=853:868
ranges["pts=4"]=1052:1067
ranges["pts=5"]=1290:1305

df=listMasses(traces, ranges; transpose=true)

scatter(df.pts,df."m48"; size=(600,300))
ylabel!("Signal (cps)")
xlabel!("Point")
```

## Create a Dict of cycles based on indications.txt file

See [`txt2dict`](@ref).

```jldoctest
julia> txt2dict("./src/examples/data/indications.txt"; pts=15, shifted=false, margin=5)
Dict{String, Tuple{Vararg{UnitRange{Int64}}}} with 5 entries:
  "pts=2" => (446:461,)
  "pts=3" => (848:863,)
  "pts=4" => (1047:1062,)
  "pts=1" => (147:162,)
  "pts=5" => (1285:1300,)
```

## List masses

```jldoctest
julia> listMasses(traces, 140:160; background=80:100, sorted=:Delta, threshold=10)
261×6 DataFrame
 Row │ mass    m_z      signal         background     Δ            δ
     │ String  Missing  Float32        Float32        Float64      Float64
─────┼───────────────────────────────────────────────────────────────────────
   1 │ m254    missing   9893.25         340.796        9552.46    29.0298
   2 │ m31     missing   8482.17        1140.05         7342.12     7.44015
   3 │ m48     missing   9729.86        2975.88         6753.98     3.26957
   4 │ m33     missing   5279.83        1212.35         4067.47     4.35503
   5 │ m43     missing   3504.12        1689.58         1814.54     2.07396
   6 │ m159    missing   2499.15         769.282        1729.87     3.24868
   7 │ m47     missing   3103.26        1546.92         1556.34     2.00609
   8 │ m163    missing   5707.84        4166.54         1541.3      1.36992
  ⋮  │   ⋮        ⋮           ⋮              ⋮             ⋮           ⋮
 255 │ m127    missing  48845.3        49173.1          -327.793    0.993334
 256 │ m19     missing  39970.9        40328.0          -357.109    0.991145
 257 │ m284    missing   8285.31        9023.13         -737.817    0.91823
 258 │ m37     missing  52604.3        53465.8          -861.445    0.983888
 259 │ m143    missing  39134.2        40226.3         -1092.13     0.97285
 260 │ m32     missing      1.13472e5      1.15391e5   -1919.2      0.983368
 261 │ m142    missing      3.12545e5      3.26627e5  -14082.0      0.956887
                                                             246 rows omitted
```

## [ON - OFF](@id ON_OFF_example)

TBD

