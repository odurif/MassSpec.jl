# How-to Guide 5: Define events

An event is anything of interest in the data. By defining events, it becomes possible to extract and focus on relevant information from the data set.

In this package, events are defined with dictionaries objects. The `key` is the event name and the `value` a `UnitRange` (corresponding to the cycle range), or a `Tuple` of `UnitRange` in case of repetetion of similiar events.

The examples below show several ways of defining events.

### Direct definition

```@example defineEvents
myEvents=Dict()
myEvents["lights=OFF, NO=0, pts1"]=260:290
myEvents["lights=ON, NO=0, pts1"]=560:590
myEvents["repeated event"]=(660:690, 710:740, 830:860)
nothing # hide
```

### Using the PTR-MS viewer interface: .json file

Using the `Analyze Trace` tool, relevant information can be defined and stored in a `json` file.

This `json` file can then be converted to `Dict` to retrieve this information for use with this program using [`json2dict`](@ref),

```@example defineEvents
using MassSpec # hide
myEvents=json2dict("../examples/data/RangeSetting.json")
```

Alternatively, a `Dict` can be converted in `json` file to be readen with the viewer using [`dict2json`](@ref),

```@example defineEvents
rawData=LoadRawData("../examples/data/data.h5") # hide
dict2json(rawData, myEvents; filePath="RangeSetting.json")
```

### From a simple ASCII .txt file

```@example defineEvents
myEvents=txt2dict("../examples/data/indications.txt")
```
