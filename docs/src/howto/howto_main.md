# [How-to Guides Introduction](@id How-to-Guides-Introduction)

The following how-to guides provide steps to work through common problems that MassSpec is targeted at solving.

## Available How-to Guides

[How-to Guide 1: Load data](@ref)

[How-to Guide 2: Mass Calibration](@ref howto_mass_calibration)

[How-to Guide 3: Define peaks](@ref)

[How-to Guide 4: Compute traces](@ref)

[How-to Guide 5: Define events](@ref)

[How-to Guide 6: Average signal](@ref)

[How-to Guide 7: Vizualization](@ref)

[How-to Guide 8: Compute the concentration](@ref)
