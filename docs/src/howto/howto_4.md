# How-to Guide 4: Compute traces

```@meta
DocTestSetup = quote
    using MassSpec
    raw = LoadRawData("./src/examples/data/data.h5")
end
```

The integration of peak signals as a function of experimental time is performed using [`computeTraces`](@ref).

```@exemple_compute_traces
traces = computeTraces(raw; peakTable="./peaktable.json", save_path="traces.jld2")
```

Results is saved within the raw data. It can also be stored in `JLD2` file.

Optional arguments:
- `save_path`: if is not specified, results are saved directly within the raw data.
- `peakTable`: if is not specified, the computation uses the peak table defined in the raw data.
