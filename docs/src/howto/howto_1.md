# How-to Guide 1: Load data

```@meta
DocTestSetup = quote
    using MassSpec
    traces = LoadTraceFile("./src/examples/data/traces.h5")
end
```

!!! note

    For the time being, the files are instrument-specific.

Depending on the specific needs of your analysis, you can choose to load a comprehensive [raw data file](@ref LoadRawData_example), which contains all information, or opt for more specific data files that focus on [time-traces](@ref LoadTraceFile_example) or [spectra data](@ref LoadSpectrumFile_example).

## [Raw data file](@id LoadRawData_example)

A raw data file encompasses all information from the mass spectrometry experiment.

Use [`LoadRawData`](@ref MassSpec.LoadRawData) to load the data, this return a [`RawData`](@ref MassSpec.RawData) object. Files are opened as read only by default, but read/write mode can be specify with the keyword `mode="r+"`. 

```@exemple
julia> raw = LoadRawData("./src/examples/data/data.h5")
MassSpec.RawData(HDF5.Dataset: /SPECdata/Intensities (file: data.h5 xfer_mode: 0), HDF5.Dataset: /PROCESSED/TraceData/PeakTableData (file: data.h5 xfer_mode: 0), HDF5.Dataset: /PROCESSED/MassCalibration/ParametersData (file: data.h5 xfer_mode: 0), 430001, 1332, [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0  …  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0], MassSpec.TraceData
  data: Array{Float32}((388, 1332)) Float32[32.052567 32.88077 … 25.203846 22.515385; 54.91282 47.229485 … 44.85769 32.852566; … ; 2.9410257 0.50128204 … 0.5089744 0.0; 0.0 8.95 … 0.5948718 1.6666666]
  masses: Dict{String, Integer}
  time: Array{Int64}((430001,)) [1, 2, 3, 4, 5, 6, 7, 8, 9, 10  …  429992, 429993, 429994, 429995, 429996, 429997, 429998, 429999, 430000, 430001]
  all_signal_each_cycle: Array{Float64}((0,)) Float64[]
  correction: Array{Float64}((430001,)) [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0  …  1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
  attributes: MassSpec.Attributes
)
```

## Other data files

### [Traces data file](@id LoadTraceFile_example)

For analyses focused on the kinetics, loading only the trace data, which is a subset of the raw data file, could be another approach.

Use [`LoadTraceFile`](@ref MassSpec.LoadTraceFile) function, return a [`TraceData`](@ref MassSpec.TraceData) structure.

```jldoctest
julia> traces = LoadTraceFile("./src/examples/data/traces.h5")
MassSpec.TraceData
  data: Array{Float32}((388, 1332)) Float32[32.052567 32.88077 … 25.203846 22.515385; 54.912823 47.229485 … 44.85769 32.852562; … ; 2.9410257 0.50128204 … 0.5089744 0.0; 0.0 8.95 … 0.5948718 1.6666666]
  masses: Dict{String, Integer}
  time: Array{Dates.Time}((1332,))
  all_signal_each_cycle: Array{Float64}((1332,)) [723737.4013466239, 722518.639450252, 723023.8032741845, 722343.341227442, 723511.0042071342, 722059.8515443206, 724261.9901789725, 723778.531118393, 724512.9994097352, 723865.2424063087  …  682396.7440678775, 683289.4742647707, 683108.9827861488, 682359.3837012649, 683183.7446026802, 683101.1770319045, 684153.3119819462, 688317.8283829689, 697317.7387784421, 699965.8971611857]
  correction: Array{Float64}((1332,)) [40555.8828125, 40358.8046875, 40289.8984375, 40274.86328125, 40211.33984375, 40459.2421875, 40281.00390625, 40264.734375, 40339.8203125, 40260.66796875  …  43223.93359375, 43295.203125, 43259.609375, 43248.234375, 42935.7734375, 42630.21875, 42256.3984375, 40281.8203125, 37386.24609375, 36576.21875]
  attributes: MassSpec.Attributes
  filePath: String "./src/examples/data/traces.h5"
```

###  [Spectrum data file](@id LoadSpectrumFile_example)

When your analysis requires examining only a mass spectrum, the simplest way is to load an ASCII file with two colums `mass` and `signal` as `DataFrame`.

```jldoctest
julia> spectrum = LoadSpectrumFile("./src/examples/data/spectra/CH3I_lights_ON.txt")
430001×2 DataFrame
    Row │ mass        signal
        │ Float64     Float64
────────┼────────────────────────
      1 │   0.16519   0.0
      2 │   0.165227  0.0
      3 │   0.165263  0.0144241
      4 │   0.1653    0.00136225
      5 │   0.165336  0.0
      6 │   0.165373  0.0
      7 │   0.165409  0.0
      8 │   0.165446  0.0143705
   ⋮    │     ⋮           ⋮
 429995 │ 388.011     0.0
 429996 │ 388.012     0.0
 429997 │ 388.014     0.0
 429998 │ 388.016     0.0
 429999 │ 388.018     0.0
 430000 │ 388.02      0.0
 430001 │ 388.021     0.0
              429986 rows omitted
```
