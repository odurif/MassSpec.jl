# How-to Guide 6: Average signal

The data is structured as a matrix, and averaging can be performed either *horizontally* or *vertically*, reducing a subset of the data to a vector. This process yield to a spectrum or to a time trace. This vector can be further reduced to a scalar, representing a specific peak signal at a particular time.

## Averaging a mass spectrum

Averaging a mass spectrum is done using [`computeSpectrum`](@ref).

An average mass spectrum can be saved into the raw data. This function can also be eventually called with a mass calibration (as argument) to increase the accuracy, as bellow:

```@example averageSignal
using MassSpec # hide
rawData = LoadRawData("../examples/data/data.h5"); # hide

m1=masses("NH3+")["m/z"]
m2=masses("C5H8O2H+")["m/z"]
m3=masses("C6H4I2H+")["m/z"]

computeSpectrum(rawData; mass_calibration=[m1,m2,m3], save=:none, progress=false);

nothing # hide
```

The parameter, `save` can be a `string` to save the result directly into an `ASCII` file. Otherwise, if `save=:auto`, then the result is saved into the raw data.

Examples are provided [`here`](@ref spectrum_example).

## [Extract mean signal for every mass and range](@id listMasses_example)

Generate a signal list (`DataFrame` type) for each event defined in `RangeSetting.json` and every molecules in the peak table.

```@example averageSignal
cycles=json2dict("RangeSetting.json")

listMasses(rawData, cycles)

nothing # hide
```

Advanced automated signal substraction can be perfomed with [`ON_OFF()`](@ref ON_OFF_example) function.
