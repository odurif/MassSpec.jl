# [How-to Guide 2: Mass Calibration](@id howto_mass_calibration)

!!! note

    To save the mass calibration, the raw data file must be opened in read/write mode. This is achieved by passing `mode="r+"` to the [`LoadRawData`](@ref) function.

Mass calibration is performed by identifying 2 or 3 peaks, corresponding to known masses.

* If 2 peaks are specified, the calibration is determined by a function of the form $m=a \times t^2 + b$, where $a$, $b$ are the 2 calibration parameters.
* If 3 peaks are specified, the calibration is determined by a function of the form $m=a \times t^c + b$, where $a$, $b$, $c$ are the 3 calibration parameters.

The 3-parameter calibration is more accurate and should be preferred. However, the 3-parameter calibration cannot be read by the PTR-MS Viewer software.

Therefore, both calibrations can also be performed automatically: one to be used preferentially with this package, and the other when the data are read using the PTR-MS Viewer.

## Step 1: Define m$_1$,m$_2$,m$_3$

```@exemple calibration
m1=masses("NH3+")["m/z"]
m2=masses("C5H8O2H+")["m/z"]
m3=masses("C6H4I2H+")["m/z"]
```

In the case where the original calibration is completely off the target, the corresponding time bin has also be defined. Otherwise, the peak corresponding to the chosen mass will be determined based on the original calibration from the acquisition.

## Step 2: Compute mass calibration

Calibration is computed using [`computeCalibrationParameters`](@ref). By default, and when 2 masses are used, calibration parameters $a$, $b$, are saved directly in the raw data file.

```@exemple calibration
raw=LoadRawData("./src/examples/data/data.h5"; mode="r+")
computeCalibrationParameters(raw, [m1,m2]; save=true)
```

If the parameter `save=false` is set, then calibration parameters are simply returned by the calibration function and in such case, no need to load the data file with the write mode.

Upon completion of the process, the averaged mass axis is stored in the data.

## Step 3: Verify the mass calibration accuracy

TBD

## Calibration and average spectrum

When computing an average spectrum using the function [`computeSpectrum`](@ref), an accurate calibration can be performed before-hand with the argument `mass_calibration=[m1,m2,m3]`. See [`examples`](@ref calibration_computeSpectrum_example).
