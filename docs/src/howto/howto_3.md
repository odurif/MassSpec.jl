# How-to Guide 3: Define peaks

Peak recognition can be automated using [`createPeakTable`](@ref).

For a given spectrum, this function identify all the peaks, and for all of them compute the exact mass, the most likely corresponding chemical formula, and the peak edges at 2σ. The peak identification is adjusted with [`proms`](@ref proms) argument.

In the example bellow, a peak table is computed for the average spectrum of the whole dataset and saved as a `JSON` file named `peaktable.json`.

```@exemple_create_peaktable
raw=LoadRawData("./src/examples/data/data.h5")
spectrum=computeSpectrum(raw, range=1:raw.numberCycles)

createPeakTable(spectrum; save="peaktable.json")
```

## [Proms argument](@id proms)

TBD: an explaination of the `proms` parameter
