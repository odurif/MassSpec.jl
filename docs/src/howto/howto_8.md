```@meta
DocTestSetup = quote
    using MassSpec
end
```

# How-to Guide 8: Compute the concentration

An online program [Flow Tube](https://flowtube-adacf5.gitlab.io/voici/render/flowtube.html?) is dedicated to compute the concentration within the flow tube.

## Alternatively, with this package

Concentrations of various molecular species involved in an experiment can be determinated with [`concentration`](@ref).

### i) Define the flows

First define the parameters for the experimental flows using [`Flows`](@ref) structure.

```jldoctest concentrationTest
julia> flows=Flows(compound=:C2H5I,air=2000,injection=5.8,dilution=904,bubbler=1.19,NO=6.01,NOinjection=5)
Flows
  compound: Symbol C2H5I
  air: Float64 2000.0
  injection: Float64 5.8
  dilution: Float64 904.0
  bubbler: Float64 1.19
  total: Float64 2005.8
  NO: Float64 6.01
  NOdilution: Float64 0.0
  NOinjection: Float64 5.0
  NO_bottle_concentration: Float64 95.4
  params: MassSpec.Params
```

### ii) Compute the concentration

Once the flows defined, calculating the concentration of each molecular species is straightforward with [`concentration`](@ref).

```jldoctest concentrationTest
julia> concentration(flows)
Dict{Any, Any} with 6 entries:
  :C2H5I         => 1.11379e-19
  :temperature_C => 293.15
  :temperature_K => 20.0
  :pressure      => 101220.0
  :NO            => 237.219
  :ρTot          => 2.50206e19
```

The [`concentration`](@ref) function can also accept direct input of flow parameters for on-the-fly calculations.

```jldoctest
julia> concentration(compound=:C2H5I,air=2000,injection=5.8,dilution=904,bubbler=1.19)
Dict{Any, Any} with 5 entries:
  :C2H5I         => 1.11379e-19
  :temperature_C => 293.15
  :temperature_K => 20.0
  :pressure      => 101220.0
  :ρTot          => 2.50206e19
```

This function returns a `Dict` containing the concentration of the precusor and nitric oxide (if specified), witin the flow tube, in parts per billion (ppb).
