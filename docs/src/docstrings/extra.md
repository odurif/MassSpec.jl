# Extra

```@docs
MassSpec.delta
MassSpec.exceldatetodate
MassSpec.findMoleculeName
MassSpec.formula
MassSpec.infoDataFrame
MassSpec.Isotope
MassSpec.isotopic_distribution
MassSpec.keysInDict
MassSpec.listCyclesKeys
MassSpec.masses_weight
MassSpec.masses
MassSpec.ON_OFF
MassSpec.searchsortednearest
MassSpec.simulate
MassSpec.whoAt
```
