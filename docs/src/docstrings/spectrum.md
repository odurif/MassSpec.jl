# Spectrum

## Compute Spectrum

```@docs
MassSpec.computeSpectrum
MassSpec.resizeSpectrum
MassSpec.spectrumInteger
MassSpec.meanSpectrum
```
