# Load data

## Load Raw Data

### Function

```@docs
MassSpec.LoadRawData
```

### Structure

```@docs
MassSpec.RawData
```

### Subfunctions

```@docs
MassSpec.averageCalibrationParametersDataSet
MassSpec.averageMassAxisDataSet
MassSpec.averageSpectrumSignalDataSet
MassSpec.timeDataSet
```

## Load Trace file

### Function

```@docs
MassSpec.LoadTraceFile
```

### Structure

```@docs
MassSpec.TraceData
```

## Load Spectrum file

### Function
```@docs
MassSpec.LoadSpectrumFile
```
