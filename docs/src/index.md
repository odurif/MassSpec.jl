# MassSpec.jl Documentation

*MassSpec.jl* is a Julia package that provides tools for Mass Spectrometry data processing.

The package is developed for research purposes in atmospheric chemistry using a PTR-TOF-MS and applies for chemical kinetics studies.

Extra features for plotting are provided in [MassSpecPlots.jl](https://gitlab.com/massspec.jl/massspecplots.jl).

## Installation

```julia-repl
julia> import Pkg; Pkg.add("MassSpec")
```

## Getting started

[How-to guides](@ref How-to-Guides-Introduction) are available as starting material.

## Code source

[MassSpec.jl](https://gitlab.com/massspec.jl/massspec.jl)

## Reporting issues

[durif@kth.se](mailto:durif@kth.se)


