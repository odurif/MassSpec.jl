using MassSpec, Documenter

makedocs(
        modules = Module[MassSpec],
        sitename = "MassSpec.jl",
        authors= "Olivier Durif",
        pages = [
            "Home" => "index.md",
            "How-to Guides" => [
                "How-To Guides Intro" => "howto/howto_main.md",
                "1 Load Files" => "howto/howto_1.md",
                "2 Mass Calibration" => "howto/howto_2.md",
                "3 Define peaks" => "howto/howto_3.md",
                "4 Compute traces" => "howto/howto_4.md",
                "5 Define events" => "howto/howto_5.md",
                "6 Average signal" => "howto/howto_6.md",
                "7 Vizualization" => "howto/howto_7.md",
                "8 Compute the concentration" => "howto/howto_8.md",
                    ],
            "Examples" => [
                "Compute Spectra" => "examples/compute_spectra.md",
                "Compute Traces" => "examples/compute_traces.md",
                "Define events" => "examples/define_events.md",
                "Kinetics" => "examples/kinetics.md"
                    ],
            "Docstrings" => [
                "calibration.jl" => "docstrings/calibration.md",
                "correlation.jl" => "docstrings/correlation.md",
                "flowTube.jl" => "docstrings/flowTube.md",
                "indications.jl" => "docstrings/indications.md",
                "listMasses.jl" => "docstrings/listMasses.md",
                "load.jl" => "docstrings/load.md",
                "peakTable.jl" => "docstrings/peakTable.md",
                "traces.jl" => "docstrings/traces.md",
                "spectrum.jl" => "docstrings/spectrum.md",
                "Extra" => "docstrings/extra.md"
                    ],
            ],
        repo = "https://gitlab.com/odurif/massspec.jl/blob/{commit}{path}#{line}",
        format = Documenter.HTML(
            #assets = ["assets/custom.css", "assets/favicon.ico"],
            prettyurls = true,
            canonical = "https://odurif.gitlab.io/MassSpec.jl/",
        ),
)

deploydocs(
     repo = "gitlab.com/odurif/massspec.jl.git",
     devbranch = "main",
)

