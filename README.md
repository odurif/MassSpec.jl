# MassSpec

[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://odurif.gitlab.io/MassSpec.jl/)

*MassSpec.jl* is package providing tools for processing mass spectrometry data.

Additional plotting tools are provided in [*MassSpecPlots.jl*](https://gitlab.com/massspec.jl/massspecplots.jl).

## Installation
```julia-repl
(@1.10) pkg> add MassSpec
```

## Reporting issues

[durif@kth.se](mailto:durif@kth.se)
