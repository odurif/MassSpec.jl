" Return the list of peaks/masses, for a given *m/z*. "
function whoAt(f::TraceData, mass::Integer; peaks=false)

	if peaks
		return collect(keys(f.peaks))[abs.(keys(f.peaks).-mass).<0.5]
	else
		masses=map(x->parse(Float64,x[2:end]),collect(keys(f.masses)))
		return sort(masses[abs.(masses.-mass).<0.5])
	end
end

function whoAt(f::TraceData, mass::String)
	if mass[1]=='m'; mass=mass[2:end] end
	whoAt(f, parse(Int,mass))
end

