## TODO> spectrumInteger! without resolution because of the same length....
"for a given Spectrum, return another Spectrum so that the signal is summed for each interger masses"
function spectrumInteger(spectrum::DataFrame; resolution=1e3, norm=false)::DataFrame

	m_min::Int64=round(Int, minimum(spectrum.mass))
	m_max::Int64=round(Int, maximum(spectrum.mass))
	mass::StepRangeLen{Float64}=m_min:m_max
	signal::Vector{Float64}=zero(mass)

    # sum the signal of the spectrum into the 'signal' vector which is the signal for each interger masses
    for i in 1:length(spectrum.mass)
        m_int=round(Int, spectrum.mass[i])
        signal[m_int-m_min+1]+=spectrum.signal[i]
    end

    # Increase the resolution of this new integer spectrum
    x::StepRangeLen{Float64}=mass[1]:1/resolution:mass[end]
    y::Vector{Float64}=zero(x)
    for m in 1:length(mass)
        y[Int((m-1)*resolution)+1]=signal[m]
    end

    if norm
        m19=findall(m->abs(m-19)<1e-3,x)[1]
        y=y/y[m19]
    end

    #return DataFrame([mass,signal],["mass","signal"]) #-> return DataFrame without zeros
    return DataFrame([x,y],["mass","signal"])

end

"""
    meanSpectrum(raw::MassSpec.RawData, range::UnitRange{Int64}; mass_calibration::Union{Nothing,Vector{Float64}}=nothing, speed::Bool=false, progress::Bool=true)::DataFrame

Computes the mean spectrum.
"""
function meanSpectrum(raw::MassSpec.RawData, range::UnitRange{Int64}; mass_calibration::Union{Nothing,Vector{Float64}}=nothing, speed::Bool=false, progress::Bool=true)::DataFrame

    if length(range)==0
        @error "Range is null. Check this parameter."
    end

    #--- Compute mean mass vector ---#

    # computation based on calibration parameters from raw data file
    if mass_calibration == nothing
        ## TODO> check if a better three calibration parameters eventually exist somewhere
        a = raw.calibrationParameters[1,range]
        b = raw.calibrationParameters[2,range]
        c = 0.5*ones(length(range))
        if typeof(raw.averageMassAxis)==Missing
            mean_mass=((collect(1:raw.cycleLength) .- mean(b)) / mean(a)) .^ (1/mean(c)) # compute mean_mass, can be improve with interpolation...
        else
            mean_mass=raw.averageMassAxis[:] # take the mean mass from the raw data, preferable than compute with calibrationParameters because for now only 2/3 parameters are saved into the data
        end
    else # recompute calibration parameters and compute the average mass vector
        a,b,c=computeCalibrationParameters(raw,mass_calibration; range=range, save=false, progress=progress)
        mean_mass=((collect(1:raw.cycleLength) .- mean(b)) / mean(a)) .^ (1/mean(c)) ## TODO> replace with a formula defined once somehwere and used everywhere???
    end

    #--------------------------------#

    #--- Compute mean signal vector ---#

    # By default, the algorithm is more precise but more computationally expensive
    return if speed
        DataFrame([mean_mass,mean(raw.data[:,range], dims=2)[:,1]],["mass","signal"])
    else # interpolate for each spectrum the signal on the averaged mass vector
        averagedSpectrumSignal=zeros(raw.cycleLength)
        p = Progress(length(range); desc="Averaging", enabled=progress)
        for i in 1:length(range)
            # Compute mass axis
            x = ((collect(1:raw.cycleLength) .- b[i]) / a[i]) .^ (1/c[i])
            # Take spectrum signal
            y = raw.data[:,i+range[1]-1]
            # Interpolate spectrum signal on averaged mass axis
            interp = linear_interpolation(x, y ,extrapolation_bc=Line()) ## allow extrapolation
            averagedSpectrumSignal=(averagedSpectrumSignal*(i-1) .+ interp(mean_mass))/i ## average during the loop iteration
            next!(p)
        end
        DataFrame("mass" => mean_mass, "signal" => averagedSpectrumSignal)
    end

    #--------------------------------#

end

function operationTwoSpectra(a::DataFrame, b::DataFrame, operation)::DataFrame

    mean_mass=(a.mass.+b.mass)./2

    ## interpolate both spectra into mean_mass
    interp_a=linear_interpolation(a.mass,a.signal,extrapolation_bc=Line()) ## allow extrapolation
    interp_b=linear_interpolation(b.mass,b.signal,extrapolation_bc=Line())

    if operation == :substract
        return DataFrame([mean_mass,interp_a(mean_mass).-interp_b(mean_mass)],["mass","signal"])
    elseif operation == :sum
        return DataFrame([mean_mass,interp_a(mean_mass).+interp_b(mean_mass)],["mass","signal"])
    elseif operation == :divide
        return DataFrame([mean_mass,interp_a(mean_mass)./interp_b(mean_mass)],["mass","signal"])
    else
        @error "Unknown operation"
        return 0
    end

end

function substractSpectrum(a::DataFrame, b::DataFrame)
    return operationTwoSpectra(a,b,:substract)
end

function sumSpectrum(a::DataFrame, b::DataFrame)
    return operationTwoSpectra(a,b,:sum)
end

function divideSpectrum(a::DataFrame, b::DataFrame)::DataFrame
    spectrum=operationTwoSpectra(a,b,:divide)
    replace!(y -> isnan(y) ? 1 : y, spectrum.signal) # 1 where NaN in case we divided by 0
    replace!(y -> y==0 ? 1 : y, spectrum.signal) # 1 as baseline where value is 0 in case we divided by 0
    #TODO delete instead ?? like using deleteat!(spectrum, findall(spectrum.signal .== NaN .|| spectrum.signal .== 0)) # delete where NaN in case we divided by 0 or 0 in case signal is 0
    return spectrum
end

## TODO: use of forceFitMasses parameter to be improved
# les masses du background fittent au 1/10 avec les masses du spectre
function fitBackgroundSpectrum(spectrum::DataFrame, background::DataFrame; forceFitMasses::Real=0.1)::Bool

    if size(spectrum)==size(background) && all(abs.(spectrum.mass.-background.mass) .< forceFitMasses)
        return true
    elseif size(spectrum)!=size(background)
        @warn "unable to remove the background, length does not fit: length(spectrum)=$(size(spectrum)) and length(background)=$(size(background)). Best would probably to try recalibrating the data."
        return false
    else
        @warn "unable to remove the background."
        return false
    end
end

"resize spectrum by summing signal and taking mean point for the corresponding mass"
function resizeSpectrum(spectrum::DataFrame; resize=5000)::DataFrame
    signal=resizeVector(spectrum.signal, resize=resize)
    dx=length(spectrum.mass)/resize
    spectrum.mass=[spectrum.mass[round(Int, (i-1)*dx+dx/2+1)] for i ∈ 1:resize]
    return spectrum
end

"""
    computeSpectrum(spectrum::DataFrame; background::Union{Nothing,DataFrame}=nothing, integer::Bool=false, resolution::Real=1e3, scale_to_1::Bool=false, relative::Bool=false, threshold::Real=0, resize=nothing, xbin::Union{Nothing,Real}=nothing, xlims::Union{Symbol,Tuple{Int64, Int64}}=:auto, forceFitMasses::Real=0.1, save::Union{String,Symbol}=:auto)

Manipulates spectrum data.

# Argument
- `spectrum::DataFrame`

# Optional arguments
- `background::Union{Nothing,DataFrame}=nothing`
- `integer::Bool=false`
- `resolution::Real=1e3`
- `scale_to_1::Bool=false`
- `relative::Bool=false`
- `threshold::Real=0`
- `resize=nothing`
- `xbin::Union{Nothing,Real}=nothing`
- `xlims::Union{Symbol,Tuple{Int64, Int64}}=:auto`
- `forceFitMasses::Real=0.1`
- `save::Union{String,Symbol}=:auto`
"""
function computeSpectrum(spectrum::DataFrame; background::Union{Nothing,DataFrame}=nothing, integer::Bool=false, resolution::Real=1e3, scale_to_1::Bool=false, relative::Bool=false, threshold::Real=0, resize=nothing, xbin::Union{Nothing,Real}=nothing, xlims::Union{Symbol,Tuple{Int64, Int64}}=:auto, forceFitMasses::Real=0.1, save::Union{String,Symbol}=:auto)

    ## Note: integer is before threshold; thus threshold is applyed on the integrated peak (when integer=true); else is the threshold is applyed on each bin

    if xlims != :auto
        filter!(row -> xlims[1] <= row.mass <= xlims[2], spectrum)
        background==nothing ? nothing : filter!(row -> xlims[1] <= row.mass <= xlims[2], background)
    end

    # reduce spectrum vector length according to xbin or resize parameters
    if isa(xbin,Float64)
        if resize != nothing; @warn "both xbin and resize defined; 'xbin' parameter take into account while 'resize' parameter ignored." end
        ## TODO: implement xbin  !!!!!!!(from size of xbin and length(spectrum) -> compute resize)
    elseif isa(resize,Int64)
        spectrum=resizeSpectrum(spectrum, resize=resize)
        background==nothing ? nothing : background=resizeSpectrum(background, resize=resize)
    end

    # if we want to sum the signal on all integer masses
    if integer
        spectrum=spectrumInteger(spectrum; resolution=resolution)
        background==nothing ? nothing : background=spectrumInteger(background; resolution=resolution)
    end

    ## TODO> revoir le critere....
    # when the signal is below the threshold -> signal at 0
    replace!(y -> y < threshold ? 0 : y, spectrum.signal)

    # normalization
    if scale_to_1
        spectrum.signal=spectrum.signal./maximum(spectrum.signal)
        background==nothing ? nothing : background.signal=background.signal./maximum(background.signal)
    end

    # remove the background; if the background fit with the spectrum
    if background==nothing
        nothing
    elseif fitBackgroundSpectrum(spectrum, background, forceFitMasses=forceFitMasses)
        spectrum = if relative
            if threshold < 10; @warn "use a threshold, otherwise the division result may be absurd." end
            divideSpectrum(spectrum,background)
        else
            substractSpectrum(spectrum,background)
        end
    else
        @error "Background and spectrum doesn't fit."
    end

    ## TODO> do corresponding checks and if "non-standard" optional arguments, add a warning that for general use it better to save raw spectrum than processed ones
    if isa(save,String) # save in a file
#         do while !isfile(save) FIX HERE
#             save=replace(save, ".txt" => "_2.txt")
#             @warn "File $save already exist. Saved as $save"
#         end

        ## TODO> save parameters in the file header
        CSV.write(save, spectrum; delim='\t')
    else # return a DataFrame
        return spectrum
    end

end

## TODO> save the spectrum into the raw data if not ".txt" specified at the end of the save name
"""
    computeSpectrum(raw::MassSpec.RawData; range::Union{Nothing,UnitRange{Int64}}=nothing, background::Union{Nothing,DataFrame,UnitRange{Int64}}=nothing, integer::Bool=false, resolution::Real=1e3, scale_to_1::Bool=false, relative::Bool=false, threshold::Real=0, resize=nothing, mass_calibration::Union{Nothing,Vector{Float64}}=nothing, xbin::Union{Nothing,Real}=nothing, xlims::Union{Symbol,Tuple{Int64, Int64}}=:auto, forceFitMasses::Real=0.1, save::Union{String,Symbol}=:auto, progress::Bool=true)

Computes spectrum from raw data.
"""
function computeSpectrum(raw::MassSpec.RawData; range::Union{Nothing,UnitRange{Int64}}=nothing, background::Union{Nothing,DataFrame,UnitRange{Int64}}=nothing, integer::Bool=false, resolution::Real=1e3, scale_to_1::Bool=false, relative::Bool=false, threshold::Real=0, resize=nothing, mass_calibration::Union{Nothing,Vector{Float64}}=nothing, xbin::Union{Nothing,Real}=nothing, xlims::Union{Symbol,Tuple{Int64, Int64}}=:auto, forceFitMasses::Real=0.1, save::Union{String,Symbol}=:auto, progress::Bool=true)

    # some checks
    if save==:auto
        if range != nothing
            @warn "Result not saved in the raw data, because the range set is a sub-set of the whole data set. Force 'save=:none' to remove this warning."
            save=:none
        elseif raw.file_mode != "r+"
            @warn "Cannot save the average spectrum in the raw data because the file is not opened in read/write mode."
            save=:none
        else
            save=:Average # save as averaged mass spectrum
        end
    end

    # check save name
    if haskey(raw.File, "MassSpec/Spectra$(String(:save))")
        # rename with adding "_$i"
        i=0; while haskey(raw.File["MassSpec/Spectra"], "$(String(:save))")
            i+=1; save=Symbol(String(:save)*"_$i")
        end
        @warn "Spectrum name already exist, thus saved as \"$(String(:save))\""
    end

    if range==nothing
        range=1:raw.numberCycles
    end
    spectrum=meanSpectrum(raw,range; mass_calibration=mass_calibration, speed=false, progress=progress)

    if isa(background, UnitRange{Int64})
        background=meanSpectrum(raw,background; mass_calibration=mass_calibration, speed=false, progress=progress)
    end

    spectrum=computeSpectrum(spectrum,background=background, integer=integer, resolution=resolution, scale_to_1=scale_to_1, relative=relative, threshold=threshold, resize=resize, xbin=xbin, xlims=xlims, forceFitMasses=forceFitMasses, save=typeof(save) == Bool ? nothing : save)

    # save or return
    if save==:none
        return spectrum
    elseif isa(save,Symbol) # save in the raw data
        if save==:Average # save as average mass spectrum
            raw.averageMassAxis[:]=spectrum.mass
            raw.averageSpectrumSignal[:]=spectrum.signal
        else # save in "MassSpec" group
            # create groups
            !haskey(raw.File, "MassSpec") ? create_group(raw.File, "PROCESSED/MassSpec") : nothing
            !haskey(raw.File["MassSpec"], "Spectra") ? create_group(raw.File, "PROCESSED/MassSpec/Spectra") : nothing
            # save spectrum
            write_dataset(raw.File, "MassSpec/Spectra/$(String(:save))", Matrix(spectrum))
        end
    else
        return spectrum
    end

end

## TODO> PROBLEM WHEN ANNOTATEE NON-INTEGER PEAKS!!!!
## TODO: change: n=all, by default
## TODO: return a DataFrame instead of a Vector of zip(m/z,intensity)?
"""
    listPeaks(spectrum::DataFrame; annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing}=nothing, num_peaks::Int64=10)

Identify num_peaks higest peaks and those with m/z explicitely specified in annotated_list.
Returns a list of Tuple(m/z, Intensity).

# Optional arguments
- `annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing}=nothing`: list of peak to identify.
- `num_peaks::Int64=10`: number of highest peaks to identify.
"""
function listPeaks(spectrum::DataFrame; annotated_list::Union{Tuple{Vararg{Int64,N} where N},Int64,Nothing}=nothing, num_peaks::Int64=10)

    isnothing(annotated_list) ? annotated_list = () : nothing
    isa(annotated_list,Int64) ? annotated_list = collect(annotated_list) : nothing

    x = spectrum.mass
    y = spectrum.signal

    # define num_peaks highest peaks in absolute value
    top_peaks = partialsortperm(abs.(y), 1:num_peaks, rev=true)
    top_peaks = zip(x[top_peaks], y[top_peaks])

    # same for peaks in annotated_list
    annotated_peaks_indices = findall(in(annotated_list), x)
    annotated_peaks = zip(x[annotated_peaks_indices], y[annotated_peaks_indices])

    return union(top_peaks,annotated_peaks)

end
