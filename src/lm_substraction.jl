function lightON_lightOFF(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    ON_OFF(raw, "light", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

function lightsON_lightsOFF(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    ON_OFF(raw, "lights", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

function lampON_lampOFF(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    ON_OFF(raw, "lamp", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

function ozoneON_ozoneOFF(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    ON_OFF(raw, "ozone", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

function ozoneOFF_ozoneON(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    OFF_ON(raw, "ozone", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

function NOON_NOOFF(raw::MassSpec.RawData, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
    ON_OFF(raw, "NO", ranges, tolerance=tolerance, norm=norm, sorted=sorted)
end

"""
    ON_OFF(raw::MassSpec.RawData, keyWord::AbstractString, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
"""
function ON_OFF(raw::MassSpec.RawData, keyWord::AbstractString, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame

    @info "$keyWord ON - $keyWord OFF"

    if norm==:PC
        norm=raw.parameters.PC_get
    end

    lm=listMasses(raw,ranges; transpose=true, norm=norm)
    ON_OFF(lm, keyWord; tolerance=tolerance, sorted=sorted)

end

# """
#     OFF_ON(raw::MassSpec.RawData, keyWord::AbstractString, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
# """
# function OFF_ON(raw::MassSpec.RawData, keyWord::AbstractString, ranges::Dict; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{Symbol,String,Vector{Float64},Nothing}=nothing, sorted::Union{String,Nothing}=nothing)::DataFrame
#
#     @info "$keyWord OFF - $keyWord ON"
#
#     if norm==:PC
#         norm=raw.parameters.PC_get
#     end
#
#     lm=listMasses(raw,ranges; transpose=true, norm=norm)
#     OFF_ON(lm, keyWord; tolerance=tolerance, sorted=sorted)
#
# end

## TODO: revoir exactement ce que fait la normalisation ici, en particulier ni normalisation
"""
    ON_OFF(lm::DataFrame, keyWord::AbstractString; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{String,Nothing}=nothing, sorted::Union{String,Nothing}=nothing, relative::Bool=false)::DataFrame

Substract matching cycles with `keyWord` ON and cycles with `keyWord` OFF.
If norm is specified, divide by the corresponding value `keyWord` ON.

# Arguments
- `lm::DataFrame`: [`listMasses`](@ref) dataframe
- `keyWord::AbstractString`: key keyWord to be substracted

# Optional arguments
- `tolerance::Union{Dict{String,Real},Nothing}=nothing`:
- `norm::Union{String,Nothing}=nothing`:
- `sorted::Union{String,Nothing}=nothing`:
- `relative::Bool=false`:
"""
function ON_OFF(lm::DataFrame, keyWord::AbstractString; tolerance::Union{Dict{String,Real},Nothing}=nothing, norm::Union{String,Nothing}=nothing, sorted::Union{String,Nothing}=nothing, relative::Bool=false)::DataFrame

    df=DataFrame()
    match_key=[replace.(s, match(r"range=\d*\.?\d+:\d+",s).match => "") for s in lm.key] # find pairs that match, but removing "range=XXX:XXX" in every key

    if !metadata(lm)["transpose"] # listMasses dataframe is not transposed

        ## TODO: completement revoir ce code dans cette partie du if........

#         # create list of cycles names with 'keyWord' ON and cycles names with 'keyWord' OFF: TODO: to be revised.....
#         cyclesName_Statut=zip(keys(colmetadata(df)),getindex.(values(colmetadata(df)),keyWord))
#         cyclesName_ON=String.([cyclesName for (cyclesName,wordStatut) in cyclesName_Statut if typeof(wordStatut)==Bool && wordStatut==true])
#         cyclesName_OFF=String.([cyclesName for (cyclesName,wordStatut) in cyclesName_Statut if typeof(wordStatut)==Bool && wordStatut==false])
#
#         for cycle_ON in cyclesName_ON
#             # find corresponding 'keyWord' OFF
#             cycle_OFF_index=findall(cyclesName_OFF.==replace(cycle_ON, "$keyWord ON" => "$keyWord OFF"))
#             # compute 'keyWord' ON - 'keyWord' OFF
#             if !isempty(cycle_OFF_index)
#                 cycle_OFF=cyclesName_OFF[cycle_OFF_index[1]]
#                 println("Operation: $cycle_ON - $cycle_OFF")
#                 if typeof(norm)==String ## warning: the normalization is taken 'keyWord' OFF, but be carreful that this signal is not affected too much by the ON/OFF. TODO> Explain this in the documentation.
#                     normalization = df[norm,cycle_OFF]
#                     @info "Normalization is taken $keyWord OFF, normalization = $normalization"
#                 else
#                     normalization = 1
#                 end
#                 df[!, replace(cycle_ON, "ON" => "Delta")]=(df[:,cycle_ON].-df[:,cycle_OFF])./normalization
#             end
#         end

    else # listMasses dataframe is transposed
        masses=select(lm, eltype.(eachcol(lm)) .<: Union{Missing, Float32, Float64}) # select float columns only

        ON_indexes=BitVector(lm[:,keyWord]) # sub-list of lm.key containing 'keyWord' and "ON"
        for (ON_index,status) in enumerate(ON_indexes)
            if status
                OFF_indexes=findall(match_key.==replace(match_key[ON_index], "$keyWord ON" => "$keyWord OFF"))
                for OFF_index in OFF_indexes  # compute 'keyWord' ON - 'keyWord' OFF

                    if relative
                        isnothing(norm) ? (@warn "be carreful with this case, check the computation here") : nothing ## checker le code, a revoir
                        @info "Operation: ($(lm.key[ON_index]) - $(lm.key[OFF_index])) / $(lm.key[ON_index])"
                        delta=(Vector(masses[ON_index,:]).-Vector(masses[OFF_index,:])) ./ Vector(masses[ON_index,:])
                    else
                        @info "Operation: $(lm.key[ON_index]) - $(lm.key[OFF_index])"
                        # substract row ON and OFF, selecting numbers only
                        delta=Vector(masses[ON_index,:]).-Vector(masses[OFF_index,:]) # do the row substraction only of Float column
                    end
                    if typeof(norm)==String  ## warning: same than bellow>> the normalization is taken 'keyWord' OFF, but be carreful that this signal is not affected too much by the ON/OFF. TODO> Explain this in the documentation.
                        delta=delta./df[OFF_index,norm]
                    end
                    delta=DataFrame(names(masses).=>delta)
                    delta[!,keyWord]=[:Delta]
                    delta[!,:key]=[replace(replace(lm.key[ON_index], "$keyWord ON" => "$keyWord Delta"), "range" => "range_ON")*", range_OFF=$(lm.range[OFF_index])"]
                    append!(df,delta)
                end
            end
        end

        # Add infoDataFrame
        df=innerjoin(select!(infoDataFrame(string.(df.key)), Not(keyWord)), df; on=:key)

    end

    return sorted == nothing ? df : sort(df, sorted)

end

