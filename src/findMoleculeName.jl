" Identify the corresponding molecule in dataBasePath for a given `peak`. "
function findMoleculeName(peak::Float64, dataBasePath::String)

	dataBase=readdlm(dataBasePath, '\t', skipstart=1)
	index=findall(x -> abs(x-peak) <= 0.005, dataBase[:,1])

	if isempty(index)
		return " "
	else
		return dataBase[index,2]
	end

end

function findMoleculeName(peak::String, dataBasePath::String)
	findMoleculeName(parse(Float64,replace(peak,","=>".")), dataBasePath)
end
