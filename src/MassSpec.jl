module MassSpec

using HDF5, DelimitedFiles, Interpolations, DataFrames, Statistics, Dates, Parameters, ProgressMeter, JSON, DataStructures, JLD2, Peaks, LsqFit, CSV

export
    computeCalibrationParameters,
    computeSpectrum,
    computeTraces,
    concentration,
    correlation,
    createPeakTable,
    delta,
    dictIntToUnitRange,
    dict2json,
    Flows,
    formula,
    infoDataFrame,
    isotopic_distribution,
    ON_OFF,
    listPeaks,
    listMasses,
    Load,
    LoadRawData,
    LoadSpectra,
    LoadSpectrumFile,
    LoadTraceFile,
    massCalibration,
    masses,
    NOppb,
    json2dict,
    read_indications,
    readIoniconPeakTable,
    substractSpectrum,
    simulate,
    txt2dict,
    txt2json,
    vaporPressure,
    whoAt,
    writePeakTable

@kwdef struct Constants
    kb::Float64 = 1.38E-23 ## Boltzmann constant (J/K)
    torrInPa::Float64 = 133.322 ## factor
    barInPa::Float64 = 1e5
    barInTorr::Float64 = 750.062
    absZero::Float64 = 273.15
end


abstract type Reactor end
abstract type File end
abstract type DataFile <: File end
abstract type FlowTube <: Reactor end
abstract type PeakTable <: DataFile end

include("./attributes.jl");
include("./load.jl");
include("./indications.jl");
include("./correlation.jl");
include("./exceldateToDate.jl");
include("./findMoleculeName.jl");
include("./flowTube.jl")
include("./interpolations.jl");
include("./lm_substraction.jl");
include("./listMasses.jl");
include("./peaktable.jl");
include("./readKeys.jl");
include("./ratio_sn.jl");
include("./spectrum_tools.jl");
include("./threshold.jl");
include("./trace_tools.jl");
include("./who.jl");
include("./delta.jl");
include("./isotopes.jl")
include("./calibration.jl")

end
