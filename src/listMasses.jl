## TODO> revoir completement, ne pas calculer "computeSpectrum", simplement lister les pics....
function listMasses(spectrum::DataFrame; background=nothing, integer=true, norm=false, relative=false,threshold=0, print=nothing)

    ## TODO: list (and computeSpectrum ???) according to peaktable
    spectre=computeSpectrum(spectrum,background=background,integer=integer,norm=norm,relative=relative,threshold=threshold,resolution=1)

    # sorted spectre by signal
    trie=sortperm(spectre.signal, rev=true)
    spectre=Spectrum(spectre.mass[trie],spectre.signal[trie])

    # print X biggest peaks
    if isa(print,Int64)
        println("mass","\t","signal")
        for (m,s) in zip(spectre.mass[1:print],spectre.signal[1:print])
            println(m,"\t",s)
        end
    end

    return spectre

end

# TODO> rename signal_XX or background_XX to XX+1
# TODO> do a struct for arguments of this function?
# TODO> revoir filter_interger
# TODO: remove norm==true?
"""
    listMasses(f::Union{MassSpec.TraceData,MassSpec.RawData}, range::Union{UnitRange{Int64},Int64}; background::Union{Nothing,UnitRange{Int64},Int64}=nothing, mass::Union{Nothing,Vector{String}}=nothing, norm::Union{Nothing,Bool,String,Vector{Float64}}=nothing, threshold::Union{Real,Nothing}=nothing, threshold_Δ::Union{Real,Nothing}=nothing, pts::Int64=20, sorted::Symbol=:Delta, filter_interger::Union{Symbol,Nothing}=nothing, transpose::Bool=false)::DataFrame

Returns a `DataFrame` with the averaged signal for each masses and every range. Result is computed based on traces signal.

If `background` is specified:
- column `Δ` = signal-background
- column `δ` = signal/background

For each masses, a corresponding *m/z* is also provided.

# Example
See [listMasses example](@ref listMasses_example).

# Arguments
- `f::[TraceData](@ref)`
- `range::Union{UnitRange{Int64},Int64}`:

# Optional arguments
- `background::Union{Nothing,UnitRange{Int64},Int64}=nothing`: background signal to average and to be substracted or divided by. If `Int64` type, this is the point where to consider this background and `pts` argument is the number of pts to average before the position.
- `mass::Union{Nothing,Vector{String}}=nothing`: list specific masses
- `norm::Union{Nothing,Bool,String,Vector{Float64}}=nothing`: the mass to be normalized by. If `true`, normalized by computed `correction` based on `m19` signal.
- `threshold::Union{Real,Nothing}=nothing`: masses with a signal bellow this threshold are not taken into account
- `threshold_Δ::Union{Real,Nothing}=nothing`: masses with a Δ signal bellow this threshold are not taken into account
- `pts::Int64=20`: Number of point to average. Does matter only if `signal` or `background` is `Int64` type.
- `sorted::Symbol=:Delta`: `:Delta`, `:delta`, `:mass`, `:signal`, `:background`
- `filter_interger::Union{Symbol,Nothing}=nothing`: `nothing`, `:none`, `:only`; filter or not integer masses
- `transpose::Bool=false`: transpose the DataFrame
"""
function listMasses(f::Union{MassSpec.TraceData,MassSpec.RawData}, range::Union{UnitRange{Int64},Int64}; background::Union{Nothing,UnitRange{Int64},Int64}=nothing, mass::Union{Nothing,Vector{String}}=nothing, norm::Union{Nothing,Bool,String,Vector{Float64}}=nothing, threshold::Union{Real,Nothing}=nothing, threshold_Δ::Union{Real,Nothing}=nothing, pts::Int64=20, sorted::Symbol=:Delta, filter_interger::Union{Symbol,Nothing}=nothing, transpose::Bool=false)::DataFrame

    typeof(f)==MassSpec.RawData ? f=f.traces : nothing
    isnothing(norm) ? norm=false : nothing
    isnothing(mass) ? mass=collect(keys(f.masses)) : nothing
    isa(range,Int64) ? range=UnitRange(range-pts,range) : nothing # convert range signal to UnitRange{Int64}
    isa(background,Int64) ? background=UnitRange(background-pts,background) : nothing # convert background to UnitRange{Int64}

    # compute corresponding m/z for every masses
    m_z=[typeof(m)!=Missing ? m["m/z"] : missing for m in masses.(mass)]

    # normalization factor
    norm_factor=if isa(norm,Bool)
		norm ? mean(f.correction[range]) : 1
	elseif isa(norm,String)
		haskey(f.masses,norm) ? mean(f.data[f.masses[norm],range]) : (@warn "$norm not in masses. The result is not normalized." 1)
    elseif isa(norm,Vector{Float64})
        mean(norm[range])
	end

	# averaging signal and background; TODO> norm the range then mean instead of mean then norm ?
	signal=[mean(f.data[f.masses[m],range])/norm_factor for m in mass]

    # compute Δ and δ
    if !isa(background,Nothing)
        background_signal=[mean(f.data[f.masses[m],background])/norm_factor for m in mass]
        Δ::Vector{Float64}=(signal.-background_signal)
        δ::Vector{Float64}=(signal./background_signal)

        order=if sorted==:Delta || sorted==:Δ
            sortperm(Δ, rev=true)
        elseif sorted==:delta || sorted==:δ
            sortperm(δ, rev=true)
        elseif sorted==:mass
            sortperm(mass, rev=true)
        elseif sorted==:signal
            sortperm(signal, rev=true)
        elseif sorted==:background
            sortperm(background_signal, rev=true)
        end
        df=DataFrame(mass=mass[order], m_z=m_z[order], signal=signal[order], background=background_signal[order], Δ=Δ[order], δ=δ[order])
        isa(threshold_Δ,Real) ? filter!(:Δ => x -> x >= threshold_Δ/norm_factor, df) : nothing
    else
        order=if sorted==:mass
            sortperm(mass, rev=true)
        else
            sortperm(signal, rev=true)
        end
        df=DataFrame(mass=mass[order], m_z=m_z[order], signal=signal[order])
    end

    !isnothing(threshold) ? filter!(:signal => x -> x >= threshold/norm_factor, df) : nothing
    transpose ? df=permutedims(df,1) : nothing

    # add metadata
    metadata!(df, "filePath", f.filePath, style=:note)
    metadata!(df, "range", range, style=:note)
    metadata!(df, "background", background, style=:note)
    metadata!(df, "mass", mass, style=:note)
    metadata!(df, "m/z", m_z, style=:note)
    metadata!(df, "norm", norm, style=:note)
    metadata!(df, "threshold", threshold, style=:note)
    metadata!(df, "threshold_Δ", threshold_Δ, style=:note)
    metadata!(df, "pts", pts, style=:note)
    metadata!(df, "sorted", sorted, style=:note)
    metadata!(df, "filter_interger", filter_interger, style=:note)
    metadata!(df, "transpose", transpose, style=:note)

    return df

end

# TODO> merge with the function above
# TODO> take into account a background somehow
"""
     listMasses(f::Union{MassSpec.TraceData,MassSpec.RawData}, range::Dict; mass::Union{Nothing,Vector{String}}=nothing, norm::Union{Nothing,Bool,String,Vector{Float64}}=nothing, threshold::Union{Real,Nothing}=nothing, threshold_Δ::Union{Real,Nothing}=nothing, pts::Int64=20, sorted::Symbol=:mass, filter_interger::Union{Symbol,Nothing}=nothing, transpose::Bool=false, info::Union{Bool,Symbol}=:auto)::DataFrame

Here `range` is a `Dict.`
"""
function listMasses(f::Union{MassSpec.TraceData,MassSpec.RawData}, range::Dict; mass::Union{Nothing,Vector{String}}=nothing, norm::Union{Nothing,Bool,String,Vector{Float64}}=nothing, threshold::Union{Real,Nothing}=nothing, threshold_Δ::Union{Real,Nothing}=nothing, pts::Int64=20, sorted::Symbol=:mass, filter_interger::Union{Symbol,Nothing}=nothing, transpose::Bool=false, info::Union{Bool,Symbol}=:auto)::DataFrame

    typeof(f)==MassSpec.RawData ? f=f.traces : nothing

    df=DataFrame()
    for (key,value) in range

        # convert UnitRange{Int64} to Tuple{UnitRange{Int64}}
        typeof(value)==UnitRange{Int64} ? value=tuple(value) : nothing

        if typeof(value)<:Tuple{Vararg{UnitRange{Int64}}}
            for v in value
                df_range=listMasses(f, v; mass=mass, norm=norm, threshold=threshold, threshold_Δ=threshold_Δ, pts=pts, sorted=:mass, filter_interger=filter_interger)
                rename!(df_range, "signal" => key*", range=$v")
                # append df_range to the larger df
                df = df==DataFrame() ? df_range : innerjoin(df, select(df_range, Not([:m_z])), on = :mass, makeunique=true) # not join 'm/z' column
            end
        else
            @error "Check the dict used: value must be of type 'UnitRange{Int64}' or 'Tuple{UnitRange{Int64}}'."
        end

    end

    info==:auto && transpose==true ? info = true : nothing

    # add info explicitely in the DF
    if transpose
        select!(df, Not([:m_z])) # not keep m/z column
        df = permutedims(df,1)
        rename!(df, :mass => :key)
        if info
            df=innerjoin(infoDataFrame(df.key, average=false), df, on = :key)
        end
    else # add 'info' in metadata
        for row in eachrow(infoDataFrame(range))
            for name in names(row)
                colmetadata!(df, row.key, name, row[name], style=:note)
            end
        end
    end

    # add metadata
    metadata!(df, "filePath", f.filePath, style=:note)
    metadata!(df, "range", range, style=:note)
    metadata!(df, "mass", mass, style=:note)
    metadata!(df, "norm", norm, style=:note)
    metadata!(df, "threshold", threshold, style=:note)
    metadata!(df, "threshold_Δ", threshold_Δ, style=:note)
    metadata!(df, "pts", pts, style=:note)
    metadata!(df, "sorted", sorted, style=:note)
    metadata!(df, "filter_interger", filter_interger, style=:note)
    metadata!(df, "transpose", transpose, style=:note)

    return df

end
