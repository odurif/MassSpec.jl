# """
#     correlation(f::TraceData; xlims::Tuple{Int64, Int64}=(1,0), threshold::Int64=0, cut::Tuple{Int64, Int64}=(0,0), range::Union{Nothing,UnitRange{Int64}}=nothing)
#
# TODO> do not use. too long to compute, compute half matrix
#
# Correlation coefficient for all masses
#
# # Argument
# - `f::TraceData`
#
# # Optionals arguments
# - `xlims::Tuple{Int64, Int64}=(1,0)`:
# - `threshold::Int64=0`:
# - `cut::Tuple{Int64, Int64}=(0,0)`:
# - `range::Union{Nothing,UnitRange{Int64}}=nothing`:
#
# Return a matrix of DataFrames
# """
# function correlation(f::TraceData; xlims::Tuple{Int64, Int64}=(1,0), threshold::Int64=0, cut::Tuple{Int64, Int64}=(0,0), range::Union{Nothing,UnitRange{Int64}}=nothing)
#
#     ## construct the matrix of correlation coefficients
#     i=1
#     for (mass,index) in f.masses
#         if i==1
#             df1=correlation(f, mass; xlims=xlims, threshold=threshold, cut=cut, range=range)
#             rename!(df1,:coefficient => mass)
#             sort!(df1, mass)
#             println(df1)
#         else
#             df2=correlation(f, mass; xlims=xlims, threshold=threshold, cut=cut, range=range)
#             sort!(df2, :coefficient)
#             insertcols!(df1, 2+i, mass => bf2[:,:coefficient])
#         end
#         i=i+1
#     end
#
#     return df1
# end

"""
    correlation(f::TraceData, mass::String; threshold::Real=0, range::Union{UnitRange{Int64},Nothing}=nothing, integerOnly::Bool=false)::DataFrame

Compute Pearson correlation coefficients between a given mass and all other masses in the dataset.

Returns a DataFrame with a single column containing the correlation coefficients, sorted in descending order.

# Arguments
- [`f::TraceData`](@ref MassSpec.LoadTraceFile)
- `mass::String`: Reference mass to correlate against all other masses

# Optionals arguments
- `threshold::Real=0`: Data points in the traces below this threshold are excluded from the correlation calculation.
- `range::Union{UnitRange{Int64},Nothing}=nothing`: Specific range of the dataset to use for correlation
- `integerOnly::Bool=false`: If `true`, only integer mass values will be considered for correlation. If `false` (the default), all mass values are considered.

See example PAGE
"""
function correlation(f::TraceData, mass::String; threshold::Real=0, range::Union{UnitRange{Int64},Nothing}=nothing, integerOnly::Bool=false)::DataFrame ## TODO> allow range to be Vector of UnitRange

    # check the mass name is conform
    mass=massName(f.masses,mass)

    range==nothing ? range=UnitRange(1,length(f.data[f.masses[mass],:])) : nothing

    # signal to be correlated
    signal=f.data[f.masses[mass],range]

    masses = integerOnly ? [mass for mass in keys(f.masses) if occursin("m",mass)] : keys(f.masses)

    # create a Dict of correlation
    correlation = Dict()
    for m in masses
        data=f.data[f.masses[m],range]
        maximum(data) > threshold ? correlation[m]=cor(signal,data) : nothing
    end

    # return a sorted DF
    return sort(DataFrame(mass=String.(keys(correlation)); corr_coef=Float64.(values(correlation))), :corr_coef)

end
