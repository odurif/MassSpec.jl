"Informations from PeakTable file"
struct Attributes <: PeakTable
    color::Dict{String,Int}
    molecules::Dict{String,String}
end

function attributes(;filePath="peak table.ionipt")

    if !isfile(filePath)
        #@warn "Please note that \"peak table.ionipt\" has not been found"
        colors,molecules=Dict(),Dict()
    else
        colors,molecules=peakTableDict(filePath=filePath)
    end

    return Attributes(colors,molecules)

end

