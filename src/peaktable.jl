"""
    writePeakTable(raw::MassSpec.RawData, path_file::String)

Writes `.json` file peak table into the raw data.

# Arguments:
- `raw::MassSpec.RawData`
- `path_file::String`: path to the peak table file
"""
function writePeakTable(raw::MassSpec.RawData, path_file::String)

    @assert raw.file_mode=="r+" "raw data file not opened in read/write mode. Please use \"mode=r+\""

    pt=readIoniconPeakTable(path_file)
    # convert pt DataFrame to String Matrix
    data=map(x->"$x", Matrix(pt[!,["label", "center", "low", "high", "ion", "ionic_isotope", "parent", "isotopic_abundance", "k_rate", "multiplier"]]))

    # delete and write "PeakTableData" dataset
    if haskey(raw.File["PROCESSED/TraceData"], "PeakTableData")
        delete_object(raw.File, "PROCESSED/TraceData/PeakTableData")
    end
    write_dataset(raw.File, "PROCESSED/TraceData/PeakTableData", permutedims(data))

    # reload
    raw.peakTable=raw.File["PROCESSED/TraceData/PeakTableData"]

    # delete and write "PeakTable" Group
    delete_object(raw.File, "PROCESSED/TraceData/PeakTable")
    create_group(raw.File, "PROCESSED/TraceData/PeakTable")
    for (i,row) in enumerate(eachrow(pt)) # for each peak in peak table
        i-=1
        if i < 10
            i="000$i"
        elseif i < 100
            i="00$i"
        elseif i < 1000
            i="0$i"
        elseif 1 < 10000
            i="$i"
        end
        name="PROCESSED/TraceData/PeakTable/$(row.label)_$i"
        create_group(raw.File, name)

        # specific datatype for string used by PTR-MS Viewer
        H5String = HDF5.Datatype(HDF5.API.h5t_create(HDF5.API.H5T_STRING, 256))

        # write attributes
        write_attribute(raw.File[name], "calc_mode", [Int32(1)]) # mode: integer
        write_attribute(raw.File[name], "center", [row.center])
        mass=masses(row.label)
        write_attribute(raw.File[name], "exact_mass", ismissing(mass) ? [row.center] : [mass["m/z"]])
        formula=create_attribute(raw.File[name], "formula", datatype(H5String), dataspace((1,))) # write as H5String
        write_attribute(formula, datatype(H5String), "")
        write_attribute(raw.File[name], "isotopic_abundance", [0.0])
        isotopic_composition=create_attribute(raw.File[name], "isotopic_composition", datatype(H5String), dataspace((1,))) # write as H5String
        write_attribute(isotopic_composition, datatype(H5String), "")
        write_attribute(raw.File[name], "k_rate", [2.0])
        label=create_attribute(raw.File[name], "label", datatype(H5String), dataspace((1,))) # write as H5String
        write_attribute(label, datatype(H5String), row.label)
        write_attribute(raw.File[name], "multiplier", [1.0])
        parent_isotope=create_attribute(raw.File[name], "parent_isotope", datatype(H5String), dataspace((1,))) # write as H5String
        write_attribute(parent_isotope, datatype(H5String), "")
        write_attribute(raw.File[name], "resolution", [0.0])
        write_attribute(raw.File[name], "shift", [0.0])

        # create borders dataset
        borders=[row.low, row.high]
        dset=create_dataset(raw.File[name], "borders", datatype(borders), dataspace(borders))
        write(dset,borders)
    end

    ## THEN
    # => TODO> recompute raw data

end

"""
    readIoniconPeakTable(filepath::String)::DataFrame

Reads peak table file (`.json` or `.ionipt`).
"""
function readIoniconPeakTable(filepath::String)::DataFrame
    json_data = JSON.parse(read(filepath, String))
    peakTable = DataFrame([[],[],[],[],[],[],[],[],[],[],[], [], [], []],["label", "center", "high", "low", "peak", "mode", "shift", "isotopic_abundance", "resolution", "k_rate", "ionic_isotope", "ion", "parent", "multiplier"])
    for entry in json_data
        name = entry["border_peak"]["name"]
        center = entry["border_peak"]["center"]
        high = entry["high"]
        low = entry["low"]
        peak = entry["peak"]
        mode = entry["mode"]
        shift = entry["shift"]
        isotopic_abundance = entry["border_peak"]["isotopic_abundance"]
        resolution = entry["border_peak"]["resolution"]
        k_rate = entry["border_peak"]["k_rate"]
        ionic_isotope = entry["border_peak"]["ionic_isotope"]
        ion = entry["border_peak"]["ion"]
        parent = entry["border_peak"]["parent"]
        multiplier = entry["border_peak"]["multiplier"]
        push!(peakTable, [name,center,high,low,peak,mode,shift,isotopic_abundance,resolution,k_rate,ionic_isotope,ion,parent,multiplier])
    end
    return sort(peakTable, :center)
end

## TODO> take this peak table from rawData file
"from peak table, create a dict of molecule names and colors and for each mass"
function peakTableDict(;filePath="peak table.ionipt")

    s = open(filePath) do file
        read(file, String)
    end

    colors=Dict(); i=0
    molecules=Dict()
    for line in split(s,"\"border_peak\":")[2:end]

        line=split.(split(line,","),":")

        name=line[1][2][2:end-1]
        center=line[2][2]
        mass="m"*string(round(parse(Float64,center), digits=3))
        if mass[end-3]!='.'; mass=mass*"0" end ## add missing 0 char
        if mass[end-3]!='.'; mass=mass*"0" end

        i=i+1
        colors[mass]=i
        molecules[mass]=name
    end

    return colors, molecules

end

"""
    createPeakTable(spectrum::DataFrame; proms::Real=10, residual_limit::Real=0.003, save::AbstractString="peaktable.json")

Creates a peak table.

    i) Peaks are defined using `Peaks.jl` and based on a minimum `proms`
    ii) A chemical formula is attributed to each peak, simply based on the minimum residual. The residual is the absolute value obtained substracting the mass of the attributed ion to mass of the actual peak.
    iii) Left and right edges are compute to 2σ

# Argument:
- `spectrum::DataFrame`: two column 'mass','signal'

# Optional arguements:
- `proms::Real=10`: minimum heigth of the peak relative to the neighbours
- `residual_limit::Real=0.003`: limit value for which a warning is display when computed residual is above (in percent, relative to the mass)
- `save::AbstractString="peaktable.json"`

"""
function createPeakTable(spectrum::DataFrame; proms::Real=10, residual_limit::Real=0.003, save::AbstractString="peaktable.json")

    mass=spectrum[:,1]
    signal=spectrum[:,2]

    ## Finds peaks
    indices, heights, data, proms, widths, edges = findmaxima(signal) |> peakproms(; min=proms) |> peakwidths()

    #plotpeaks(mass, signal, peaks=indices, prominences=true, widths=true)

    ## TODO> Fitter une gaussienne et trouver le max local de la gaussienne

    ## TODO> calculer l'air sous la courbe

    ## Get tabular of all possible masses
    chemFormula=readdlm(joinpath(dirname(pathof(MassSpec)), "data", "peaklist.txt"))[:]
    mz=[]

    ## compute the corresponding m/z of all chemFormula
    for f in chemFormula
        push!(mz,masses(string(f))["m/z"])
        if typeof(masses(string(f))["m/z"])==Missing
            @error "Problem with $f. Check your peak list."
        end
    end

    ## Create a df Peak table
    df=DataFrame(mz=Float64[],ion=AbstractString[],residual=Float64[],leftedge=Int64[],rightedge=Int64[])
    for (i,peak) in enumerate(indices)

        # Find the most likely peak, minimizing the residual
        idx=argmin(abs.(mz .- mass[peak]))
        ion=chemFormula[idx]

        # push the name as 'ion mass_weight' instead of the ion formula when deviation is too high
        residual=mz[idx] .- mass[peak]
        if abs(residual) > residual_limit
            ion="m$(round(mass[peak], digits=3))"
            residual=0
        end

        # Validate the ion's name: check ion is not already defined in the df, and if so, rename them
        ion_new=ion
        j=0
        while (ion_new in df.ion)
            j+=1
            ion_new=ion*"_$j"
        end
        ion=ion_new

        # push the result into the peak table
        push!(df,[round(mass[peak], digits=3), ion, residual, Int(round(edges[i][1])), Int(round(edges[i][2]))])

    end

    # Compute edges at 2 sigma
    σ=Int.(round.((df.rightedge.-df.leftedge)/2))
    df[:,"leftedge_2σ"]=df.leftedge.-σ
    df[:,"rightedge_2σ"]=df.rightedge.+σ

    # write peaktable; ## TODO> calculer/adapter la resolution
    if isfile(save)
        @warn "$save already exist. Save operation ignored."
    else
        open(save,"a") do io
            print(io,"[")
            for (i,row) in enumerate(eachrow(df))
                i == 1 ? nothing : print(io,",")
                print(io,"{\"border_peak\":{\"name\":\"$(row.ion)\",\"center\":$(row.mz),\"ion\":\"\",\"ionic_isotope\":\"\",\"parent\":\"\",\"isotopic_abundance\":0,\"k_rate\":2,\"multiplier\":1,\"resolution\":0},\"low\":$(mass[row.leftedge_2σ]),\"high\":$(mass[row.rightedge_2σ]),\"peak\":[],\"mode\":1,\"shift\":0}")
            end
            print(io,"]")
        end
    end

end

"""
    createPeakTable(raw::MassSpec.RawData; proms::Real=10, residual_limit::Real=0.003, save::AbstractString="peaktable.json")

Creates a peak table using the averaged spectrum of the raw data.
"""
function createPeakTable(raw::MassSpec.RawData; proms::Real=10, residual_limit::Real=0.003, save::AbstractString="peaktable.json")
    spectrum=DataFrame("mass" => raw.averageMassAxis[:], "signal" => raw.averageSpectrumSignal[:])
    createPeakTable(spectrum ; proms=proms, residual_limit=residual_limit, save=save)
end
