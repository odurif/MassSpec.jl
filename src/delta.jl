## TODO> add a key in "info" taking info_ON.key or info_OFF.key and changing "ON" or "OFF" by "Δ"
## TODO> add norm_mass
"""
    delta(df::DataFrame)::DataFrame

Substract two rows "ON" - "OFF"

# Argument
- `df::DataFrame`

# Optional argument
- `norm::Symbol`: `:none`, `:ON`, `:OFF`

Rows must have exactly the same `key`, so that "ON" must match "OFF"
"""
function delta(df::DataFrame; norm::Symbol=:none)::DataFrame

    data=DataFrame()

    for k_ON in df."key"[occursin.("ON", df."key")] # iterate over all "ON" keys

        k_OFF=df."key"[df."key".==replace(k_ON, "ON" => "OFF")] # search for the corresponding "OFF" key
        if !isempty(k_OFF)

            # select masses lights ON
            df_ON=subset(df, :key => ByRow(==(k_ON)))
            data_ON=select(df_ON, [name for name in names(df_ON) if isa(df_ON[!,name], Vector{Float32})])
            info_ON=select(df_ON, [name for name in names(df_ON) if !isa(df_ON[!,name], Vector{Float32})])
            # select masses lights OFF
            df_OFF=subset(df, :key => ByRow(==(k_OFF[1])))
            data_OFF=select(df_OFF, [name for name in names(df_OFF) if isa(df_OFF[!,name], Vector{Float32})])
            info_OFF=select(df_OFF, [name for name in names(df_OFF) if !isa(df_OFF[!,name], Vector{Float32})])

            # compute delta
            delta=data_ON.-data_OFF
            # normalization
            if norm==:ON
                delta=delta./data_ON
            elseif norm==:OFF
                delta=delta./data_OFF
            elseif norm != :none
                @warn "norm argument not recognized"
            end
            # keep columns with similar values
            info=select(info_ON, [name for name in names(info_ON) if info_ON[:,name]==info_OFF[:,name]])
            # add ranges
            insertcols!(info,1, :rangeON => info_ON.range, :rangeOFF => info_OFF.range)

            # Add info and delta as a row to the resulting DF ## TODO: what's happen in case of 'missing' information....
            append!(data,crossjoin(info,delta))

        end

    end

    return data

end
