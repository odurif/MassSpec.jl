"Returns a list of keys, adding the range"
function listCyclesKeys(cycles::Dict)::Vector{String}
    cyclesKeys::Vector{String}=[]
    for (key,value) in cycles
        if typeof(value)<:Tuple
            for v in value
                push!(cyclesKeys,key*", range=$v")
            end
        elseif typeof(value)<:UnitRange
            push!(cyclesKeys,key*", range=$value")
        else
            @error "Type of $value is $(typeof(value)). Check why."
        end
    end
    return cyclesKeys
end

"Returns a list of names, reading keys of cycles"
function keysInDict(cyclesKeys::Vector{String})::Vector{String}

    keyWords::Vector{String}=[]
    for k in cyclesKeys
        k=replace(k, ";" => ",")
        for r in split.(k, ",")
            substring=split.(r, "=")
            if length(substring)==1
                ## TODO: do something more generic, find the 'string' before ON or OFF.
                if occursin("lights ", substring[1]) || occursin("lights ", substring[1]) ## TODO: case match
                    key="lights"
                elseif occursin("light ", substring[1]) || occursin("light ", substring[1])
                    key="lights"
                elseif occursin("NO ", substring[1]) || occursin("NO ", substring[1])
                    key="NO"
                elseif occursin("lamp ", substring[1]) || occursin("lamp ", substring[1])
                    key="lamp"
                elseif occursin("ozone ", substring[1]) || occursin("ozone ", substring[1])
                    key="ozone"
                else
                    key="info"
                end
            elseif length(substring)==2
                key=strip(substring[1])
            else
                @warn "Anormal string: $k containing more than one \"=\". It might be necessary to use \",\" or \";\" as separator."
            end
            push!(keyWords,key)
        end
    end
    push!(keyWords,"key")
    return unique(keyWords)
end


"""
    infoDataFrame(cycles::Union{Dict,Vector{String}}; average::Bool=false)::DataFrame

Convert Dictionnary of cycles of a vector of string into DataFrame.
"""
function infoDataFrame(cycles::Union{Dict,Vector{String}}; average::Bool=false)::DataFrame

    cyclesKeys = typeof(cycles) <: Dict ? listCyclesKeys(cycles) : cycles # create a vector of string containing all keys words
    keyWords=keysInDict(cyclesKeys) # get all relevant 'key words' into a list
    keyWordsDict=Dict((key=>[] for key in keyWords)) # initialization of the dict of 'key words' (this one will be converted into the df)

    for (i,k) in enumerate(cyclesKeys) ## use a counter for counting the length of the vector; if no 'value' add "missing" to keep all vectors to the same length at each step
        push!(keyWordsDict["key"],k)

        for substring in split.(split(replace(k, ";" => ","), ","),"=") ## split the 'dictionnary key'

            if length(substring)==1
                if occursin(" ON",substring[1])
                    word=strip(split.(substring[1], "ON")[1])
                    push!(keyWordsDict[word],true)
                elseif occursin(" OFF",substring[1])
                    word=strip(split.(substring[1], "OFF")[1])
                    push!(keyWordsDict[word],false)
                elseif occursin(" Delta",substring[1])
                    word=strip(split.(substring[1], "Delta")[1])
                    push!(keyWordsDict[word],:Delta)
                else # 'info' column is anything else
                    haskey(keyWordsDict, "info") ? nothing : keyWordsDict["info"]=Vector{Any}(missing, length(cyclesKeys))
                    push!(keyWordsDict["info"],substring[1])
                end
            elseif length(substring)==2
                substring[1]=strip(substring[1])
                for key in keyWords
                    if key==substring[1]
                        try
                            if substring[1]=="range" || substring[1]=="range_ON" || substring[1]=="range_OFF" # specific case for "range", must match UnitRange
                                range=parse.(Int64, split(match(r"\d*\.?\d+:\d+", substring[2]).match, ":"))
                                push!(keyWordsDict[key],UnitRange(range[1]:range[2]))
                            else
                                push!(keyWordsDict[key],parse(Float64, match(r"\d*\.?\d+", substring[2]).match)) ## and convert string in Float64
                            end
                        catch
                            @error "TODO"
                            ## TODO: revoir, generaliser pour n'importe quel mot clef ici
#                             if occursin("light",lowercase(substring[1]))
#                                 if uppercase(strip(substring[2]))=="ON"
#                                     push!(keyWordsDict["lights"],true)
#                                 elseif uppercase(strip(substring[2]))=="OFF"
#                                     push!(keyWordsDict["lights"],false)
#                                 elseif uppercase(strip(substring[2]))=="DELTA"
#                                     push!(keyWordsDict["lights"],:Delta)
#                                 else
#                                     @warn "in $k, it is not specified if lights are ON or OFF..."
#                                     push!(keyWordsDict["lights"],missing)
#                                 end
#                             elseif occursin("NO ",uppercase(substring[1]))
#                                 if uppercase(strip(substring[2]))=="ON"
#                                     push!(keyWordsDict["NO"],true)
#                                 elseif uppercase(strip(substring[2]))=="OFF"
#                                     push!(keyWordsDict["NO"],false)
#                                 elseif uppercase(strip(substring[2]))=="DELTA"
#                                     push!(keyWordsDict["NO"],:Delta)
#                                 else
#                                     @warn "in $k, it is not specified if NO is ON or OFF..."
#                                     push!(keyWordsDict["NO"],missing)
#                                 end
#                             else
#                                  "No Float64 found in $(substring[1]) (arguments is '$(substring[2])'). Parameter ignored!"
#                             end
                        end
                    end
                end
            else
                @warn "Anormal string: $k containing more than one \"=\". It might be necessary to use \",\" or \";\" as separator."
            end

        end

        ## add "missing" if no 'keys with keyWordsDict' to keep all vectors to the same length at each step
        for key in keyWords
            if !(key in keys(keyWordsDict) && length(keyWordsDict[key]) == i)
                push!(keyWordsDict[key],missing)
            end
        end

    end

    # in case of multiples cycles for one defined entrance in the Dict
#     if !average && typeof(cycles) <: Dict
#         i=0
#         for k in keys(cycles)
#             i+=1
#             change=0
#             for j in 1:(isa(cycles[k],UnitRange{Int64}) ? 1 : length(cycles[k]))-1
#                 [insert!(keyWordsDict[key], i, keyWordsDict[key][i]) for key in keyWords]
#                 insert!(keyWordsDict["range"],i,keyWordsDict["range"][i][j]); change=1  ## TODO> checker qu'on le UnitRange correspond bien a la bonne ligne
#                 i+=1
#             end
#             change==1 ? keyWordsDict["range"][i]=keyWordsDict["range"][i][end] : nothing
#         end
#     end

    return DataFrame(keyWordsDict)

end
