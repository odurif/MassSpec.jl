## TODO: structure not used for now
# "Indications is a 'lab book file' containing the experiments infornations and used to comment a kinetic trace"
# struct Indications <: File
#     cycles::Vector{Int64}
#     comments::Vector{String}
#     colors::Vector{Symbol} ## TODO> color into MassSpecPlots
# end
#
# ## TODO: print the ignored files 'comments' too (TODO: in read_indications, distinguish between 'usefull comments for trace' and 'not usefull comments for trace')
# "print the indications"
# function comments(indications::Indications)
#     for (cycle,comment) in zip(indications.cycles,indications.comments)
#         println(string(cycle)*": "*comment)
#     end
# end

"""
    json2dict(filePath::String)::Dict

# Argument:
- `filePath::String`: Path to the rangeSettings `.json` file
"""
function json2dict(filePath::String)::Dict
    dict=Dict{String,Vector{UnitRange{Int64}}}()
    open(filePath,"r") do f
        for range in JSON.parse(f)
            key=strip(range["Name"])
            if !haskey(dict, key)
                dict[key]=[]
            end
            push!(dict[key],range["Cycle Min"]:range["Cycle Max"])
        end
    end
    return Dict(k => Tuple(v) for (k,v) in dict)
end

"""
    dict2json(raw::MassSpec.RawData, cycles::Dict; filePath::String="RangeSetting.json")

Transform a dictionnary to a `.json` File.

# Arguments:
- `raw::MassSpec.RawData`
- `cycles::Dict`

# Optional argument:
- `filePath::String="RangeSettings.json"`: name of the `.json` file in which to save the dictionnary of cycles
"""
function dict2json(raw::MassSpec.RawData, cycles::Dict; filePath::String="RangeSetting.json")

    if !(raw.excelTime==nothing) && !(raw.realTime==nothing)
        if !isfile(filePath)

            # convert the 'Dict' cycles as pairs: "cycles" => UnitRange, stored in 'p'
            p=[]
            for (k,v) in cycles
                if typeof(v)<:Tuple
                    for vv in v
                        push!(p, (k => vv))
                    end
                elseif typeof(v)==UnitRange{Int64}
                    push!(p, (k => v))
                else
                    @error "This case should not exist"
                end
            end
            sort!(p; by = x -> x[2])

            open(filePath, "w") do file
                write(file, "[\n")
                i=0
                for (k,v) in p
                    i+=1
                    write(file, "  {\n")
                    write(file, "    \"Cycle Min\":$(v[1]),\n")
                    write(file, "    \"Cycle Max\":$(v[end]),\n")
                    write(file, "    \"Name\":\"$k\",\n")
                    write(file, "    \"Time Min\":$(raw.excelTime[v[1]]),\n")
                    write(file, "    \"Time Max\":$(raw.excelTime[v[end]]),\n")
                    write(file, "    \"Time rel Min\":$(raw.realTime[v[1]]),\n")
                    write(file, "    \"Time rel Max\":$(raw.realTime[v[end]]),\n")
                    write(file, "    \"StartIndex\":$(v[1]-1),\n")
                    write(file, "    \"EndIndex\":$(v[end]-1)\n")
                    length(p)==i ? write(file, "  }\n") : write(file, "  },\n")
                end
                write(file, "]\n")
            end
        else
            error("File $filePath already exist.")
        end
        println("Successful! File save as $filePath")
    else
        @error "excelTime and/or realTime not defined. dict2JSON() function is not implemented for old raw data."
    end
end

"""
    txt2dict(filePath::String; pts::Int64=20, shifted::Bool=false, margin::Int64=0, lastPoint::Union{Missing,Int64}=missing)::Dict

# Argument:
- `filePath::String`

# Optional argument:
- `pts::Int64=20`: number of pts to take into account to construct the UnitRange defining cycles to average
- `shifted::Bool=false`: if true key is shifted by one line (often interesting to define the event once is finished, not started)
- `margin::Int64=0`: number of cycles to be removed to take a margin
- `lastPoint::Union{Missing,Int64}=missing`: force last point if shifted

Transform ASCII `txt` file into `Dict` object.

For `indications.txt` file, line structure has to be:
`XXX; Comment ## Color`
where XXX is a cycle number and this can also be an `UnitRange` (i.e. 300:330).

Note: if `UnitRange` defined in the file, then force shifted to be false.

# Algorithm:
1. From the `indications.txt` file do a list of `key` and a list of `value`
2. If `shifted=true`, roll `key` compared to `value` and don't keep the last point for which the `key` must be `END`
3. Transform `key` and `value` as `Dict` of `Tuples`
"""
function txt2dict(filePath::String; pts::Int64=20, shifted::Bool=false, margin::Int64=0, lastPoint::Union{Missing,Int64}=missing)::Dict

    v,k=[],[] # value and key
    keep=[] # bitvector, to keep or not the line

    # do a list of 'key' and a list of 'value'
    open(filePath, "r") do f

        # Read the indications files according to a specific pattern. Refer about this pattern in the manual.
        while ! eof(f) # read till end of file
            s = readline(f)
            if occursin(";",s)
                # define value,key
                value,key=split(s,";", limit=2)
                if '#'==value[1] # line start with '#'
                    value=value[2:end] # keep the value
                    push!(keep,false) # but ultimately, don't keep this cycle
                else
                    push!(keep,true)
                end
                value,key=strip(value),strip(key)
                if value=="0" && !shifted
                    @error "$key value is 0; shifted should be true ?"
                end
                # convert value
                if occursin(r"^\d+$",value) # value is an integer
                    value=parse(Int64,value)
                elseif occursin(r"^\d+:\d+$",value) # value is an UnitRange
                    value=parse(Int64,split(value,":")[1]):parse(Int64,split(value,":")[2])
                    shifted=false # force shifted to be false
                end
                push!(v,value)
                push!(k,key)
            end
        end

    end

    # if shifted, roll key compared  to value and don't keep the last point
    if shifted
        @info "Shifted is $shifted"
        if k[end] != "END"  && ismissing(lastPoint)
            @error "Last point not defined"
        elseif !ismissing(lastPoint)
            k[end]=lastPoint # force last point
        end
        k=k[1:end-1]
        v=v[2:end]
    end

    ## TODO: in case group=false
    # convert k,v as dict
    dict=Dict{String,Vector{UnitRange{Int64}}}()
    for (key,value,keep) in zip(k,v,keep)
        if keep
            haskey(dict,key) ? nothing : dict[key]=[]# create a new dict entry
            if typeof(value)==Int64  # case value is an Integer
                push!(dict[key],value-pts-margin:value-margin) # remove 'margin' number of cycles to take a margin
            elseif typeof(value)==UnitRange{Int64} # case value is a UnitRange
                push!(dict[key],value.-margin) # remove 'margin' number of cycles to take a margin
            end
        end
    end
    # convert the Dict vector to Dict tuple
    result=Dict{String,Tuple{Vararg{UnitRange{Int64},N} where N}}()
    for key in keys(dict)
        result[key]=Tuple(dict[key])
    end

    return result

end

"""
    txt2json(rawData::MassSpec.RawData,txtFilePath::String, jsonFilePath::String; pts::Int64=20, shifted::Bool=false, margin::Int64=0, lastPoint::Union{Missing,Int64}=missing)
"""
function txt2json(rawData::MassSpec.RawData,txtFilePath::String, jsonFilePath::String; pts::Int64=20, shifted::Bool=false, margin::Int64=0, lastPoint::Union{Missing,Int64}=missing)
    cycles=txt2dict(txtFilePath; pts=pts, shifted=shifted, margin=margin, lastPoint=lastPoint)
    dict2json(rawData, cycles::Dict; filePath=jsonFilePath)
end

# """
#    json2txt(txtFilePath::String, jsonFilePath::String; group::Bool=false)
# """
# function json2txt(txtFilePath::String, jsonFilePath::String; group::Bool=false)
#     cycles=json2dict(jsonFilePath; group=group)
#     #dict2txt() ## To be implemented
# end
