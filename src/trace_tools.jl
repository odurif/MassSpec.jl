function massName(list::Dict,mass::String) ## revoir et renvoyer un Bool ???

    if mass in keys(list)
        return mass

    elseif mass[1]=='m'

        mass=parse(Float64,replace(mass,","=>"."))

        if mass[end-3]!='.'; mass=mass*"0" end
        if mass[end-3]!='.'; mass=mass*"0" end
        if mass[end-3]!='.'; mass=mass*"0" end

        if mass in keys(list)
            return mass
        end

    end

    @error "Mass $mass not found. Check the mass."

end

function mymax(v)
    m = v[1]
    for i in 2:length(v)
        z = v[i]
        z<m || (m=z)
    end
    m
end

function dictIntToUnitRange(dict::Dict; pts::Int64=19)::Dict

    return Dict(k => collect(i-pts:i for i in v) for (k,v) in dict)

end

"Find nearest index of a sorted array"
function searchsortednearest(a,x)
   idx = searchsortedfirst(a,x)
   if (idx==1); return idx; end
   if (idx>length(a)); return length(a); end
   if (a[idx]==x); return idx; end
   if (abs(a[idx]-x) < abs(a[idx-1]-x))
      return idx
   else
      return idx-1
   end
end


## TODO: baseline : :none, :linear
"""
    computeTraces(raw::MassSpec.RawData; peakTable::Union{String,Nothing}=nothing, save_path::Union{Nothing,String}=nothing)

Compute traces based on masses defined in the peak table.

Peak table saved in the raw data is used if no peak table path specified.

The output can be saved in `JLD2` by passing a save path as an argument. Otherwise, it is saved into the raw data file.

Argument:
- `raw::MassSpec.RawData`: Raw data

Optional arguments:
- `peakTable::Union{String,Nothing}=nothing`: Peak table path
- `save_path::Union{Nothing,String}=nothing`: Path to save traces in a `JLD2` file (.jld2)
"""
function computeTraces(raw::MassSpec.RawData; peakTable::Union{String,Nothing}=nothing, save_path::Union{Nothing,String}=nothing)

    if typeof(peakTable)==String # first: write peak table into raw data
        writePeakTable(raw, peakTable)
    end

    # load peak table from raw data
    pt=DataFrame(permutedims(raw.peakTable[:,:]), raw.File["PROCESSED/TraceData/PeakTableInfo"][:])

    traces = zeros(Float32, size(pt)[1], raw.numberCycles)
    @showprogress for i in 1:raw.numberCycles

        # create mass axis for each spectrum following the calibration: TODO> use 3 calibration parameters if exist!!!
        #massAxis=(((1:raw.cycleLength) .- raw.calibrationParameters[2,i]) ./ raw.calibrationParameters[1,i]) .^ 2
        massAxis=raw.averageMassAxis[:] # meanwhile use the averaged mass

        spectrum=raw.data[:,i]
        for (j,peak) in enumerate(eachrow(pt))
            idx_low=searchsortednearest(massAxis,parse.(Float64,peak.low)) # in-house optimized function to find the nearest index
            idx_high=searchsortednearest(massAxis,parse.(Float64,peak.high))
            traces[j,i]=sum(spectrum[idx_low:idx_high])
        end

    end

    if save_path != nothing && save_path[end-4:end] == ".jld2" # save into .jld2 file
        traces=Dict((mass => traces[i,:] for (i,mass) in enumerate(keys(pt)))) # transform Matrix into Dict
        save(save_path, traces)
    else # save into raw data
        # rewrite `RawData`
        delete_object(raw.File, "PROCESSED/TraceData/RawData")
        write_dataset(raw.File["PROCESSED/TraceData/"], "RawData", traces)
        # rewrite `RawDataInfo`
        delete_object(raw.File, "PROCESSED/TraceData/RawInfo")
        write_dataset(raw.File["PROCESSED/TraceData/"], "RawInfo", pt.label)
        #info=create_dataset(raw.File["PROCESSED/TraceData"], "RawInfo", datatype(H5String), dataspace((size(pt)[1],))) ## TODO: SAVE AS H5String instead of String, doesn't work for now....
        #HDF5.API.h5a_write(info, datatype(H5String), map(x -> codeunits("$x"), pt.label)) ## write row.label as ASCII
        # rewrite `TraceMasses`
        delete_object(raw.File, "PROCESSED/TraceData/TraceMasses")
        write_dataset(raw.File["PROCESSED/TraceData/"], "TraceMasses", parse.(Float32,pt.center))
    end

end


