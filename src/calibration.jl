"""
    massCalibration(fileName::String; masses::Union{Vector{Float64},NTuple,Missing}=missing, progress::Bool=true)

    Automate the mass calibration.

    i) Load Data in read/write mode
    ii) Perform 2 parameter calibration
    iii) Perform 3 parameter calibration
"""
function massCalibration(fileName::String; masses::Union{Vector{Float64},NTuple,Missing}=missing, progress::Bool=true)

    rawData=LoadRawData(fileName; mode="r+")

    if ismissing(masses) # TODO: define masses based on what is into the raw Data ?
        masses=[21.0221,59.04914,142.9308]
    elseif length(masses)!=3
        @warn "3 masses must be defined...."
    end
    @info "Masses for the calibration are $masses."

    # 2 parameter calibration to be readable with the PTR-MS Viewer
    computeCalibrationParameters(rawData, [minimum(masses),maximum(masses)], progress=progress)

    # 3 parameter calibration, more accurate
    computeCalibrationParameters(rawData, masses, progress=progress)

end
"""
    computeCalibrationParameters(raw::MassSpec.RawData, masses::Union{Vector{Float64},NTuple}; timebin::Union{Vector{Int64},NTuple,Missing}=missing, m_slippage::Real=0.4, save::Bool=true, range::Union{Missing,UnitRange{Int64}}=missing, progress::Bool=true)

    Two or three parameters calibration.

    Input: rawData, Vector of _m/z_.

    Note: if no timebin specified, use the original calibration to provide an estimated timebin.

    * If 2 calibration-parameters:
        Compute calibration parameters: ``a``,``b``, such that: ``t = a\\sqrt{m} + b <=> m = (\\frac{t-b}{a})^2``.
        Thus:   ``b = \\frac{t_2*\\sqrt{m_1}-t_1*\\sqrt{m_2}}{\\sqrt{m_1}-\\sqrt{m_2}}``
                ``a = \\frac{t_1-b}{\\sqrt{m_1}}``

        - Find the position of the maximum signal (peak) for each mass, into the m_slippage defined.
        - Then compute optimal ``a``,``b`` parameters
        - Write result into `rawData.averageMassAxis` or return ``a``,``b``

    * If 3 calibration-parameters:
        calib_mass_formula=a*(m)^c+b
        - Find the position of the maximum signal (peak) for each mass, into the m_slippage defined.
        - Use least square method to compute these parameters
        - Write result into `PROCESSED_MassSpec/CalibrationParameters` or return ``a``,``b``,``c``

TBD:
* case with more than 3 masses
* compute RMS

Arguments:
- `raw::MassSpec.RawData`
- `masses::Union{Vector{Float64},NTuple}`
Optional arguments:
- `timebin::Union{Vector{Int64},NTuple,Missing}=missing`: corresponding timebin
- `m_slippage::Real=0.4`: tolerance to find the maximum of a peak in _m/z_
- `save::Bool=true`: save calibration into the raw data
- `range::Union{Missing,UnitRange{Int64}}=missing`: range where to perform the calibration, all the file is taken if set to `missing`
- `progress::Bool=true`
"""
function computeCalibrationParameters(raw::MassSpec.RawData, masses::Union{Vector{Float64},NTuple}; timebin::Union{Vector{Int64},NTuple,Missing}=missing, m_slippage::Real=0.4, save::Bool=true, range::Union{Missing,UnitRange{Int64}}=missing, progress::Bool=true)

    if save && raw.file_mode != "r+"
        @error "Load raw data with \"mode='r+'\" as argument."
    end
    ismissing(range) ? range = UnitRange(1:raw.numberCycles) : nothing

    # calibration parameters
    a=zeros(length(range))
    b=zeros(length(range))
    c=zeros(length(range))

    # reference masses for calibration
    m1=masses[1]
    m2=masses[2]

    # Define timebin range where to look in to find the maxium peak, for each mass
    if !ismissing(timebin) || ismissing(raw.averageMassAxis)
        if ismissing(timebin)
            @error "Data not processed yet, specify corresponding timebin!"
        end
        ## TODO convert m_slippage to t_slippage reliably, abritrary 0.5% factor for now (use calibration formula to determine where to look compare to the peak)
        t1_lower=Int(round(timebin[1]*0.995))
        t1_upper=Int(round(timebin[1]*1.015))
        t2_lower=Int(round(timebin[2]*0.995))
        t2_upper=Int(round(timebin[2]*1.015))
    else # If timebin not specified, find them based on the current averageMass axis
        averageMass=raw.averageMassAxis
        t1_lower=findmin(abs.(averageMass.-(m1-m_slippage)))[2]
        t1_upper=findmin(abs.(averageMass.-(m1+m_slippage)))[2]
        t2_lower=findmin(abs.(averageMass.-(m2-m_slippage)))[2]
        t2_upper=findmin(abs.(averageMass.-(m2+m_slippage)))[2]
    end
    t1=zeros(length(range))
    t2=zeros(length(range))

    if length(masses)==2 # two parameters calibration

        p = Progress(length(range); desc="Calibrating", enabled=progress)
        for i in 1:length(range)
            # Find corresponding optimal timebin
            t1[i]=t1_lower+findmax(raw.data[t1_lower:t1_upper,i+range[1]-1])[2]-1
            t2[i]=t2_lower+findmax(raw.data[t2_lower:t2_upper,i+range[1]-1])[2]-1
            mean_t1=
            # Compute parameters a,b
            b[i]=(t2[i]*m1^0.5-t1[i]*m2^0.5)/(m1^0.5-m2^0.5)
            a[i]=(t1[i]-b[i])/m1^0.5 ## or a[i]=(t2[i]-b[i])/m2^0.5
            c[i]=0.5 # fix parameter in such case
            next!(p)
        end

        if save
            raw.calibrationParameters[1,range]=a # "PROCESSED/MassCalibration/ParametersData"
            raw.calibrationParameters[2,range]=b

            if range==1:raw.numberCycles
                raw.File["PROCESSED/AverageSpectrum/Masscal/ParametersData"][1,1]=mean(a) # Warning this value is different of mean(raw.calibrationParameters[1,range]) (because conversion Float32)
                raw.File["PROCESSED/AverageSpectrum/Masscal/ParametersData"][2,1]=mean(b) # Same here with b

                raw.File["PROCESSED/MassCalibration/SettingsData"][1,1]=m1
                raw.File["PROCESSED/MassCalibration/SettingsData"][1,2]=m2
                raw.File["PROCESSED/MassCalibration/SettingsData"][2,1]=mean(t1)
                raw.File["PROCESSED/MassCalibration/SettingsData"][2,2]=mean(t2)
                raw.File["PROCESSED/MassCalibration/SettingsData"][3,1]=m1 # extra column used by the Viewer
                raw.File["PROCESSED/MassCalibration/SettingsData"][3,2]=m2
            end
        end

    elseif length(masses)==3 # three parameters calibration
        m3=masses[3]

        if !ismissing(timebin) || ismissing(raw.averageMassAxis)
            t3_lower=Int(round(timebin[2]*0.995)) ## TODO: to be improved!
            t3_upper=Int(round(timebin[2]*1.015))
        else
            t3_lower=findmin(abs.(averageMass.-(m3-m_slippage)))[2]
            t3_upper=findmin(abs.(averageMass.-(m3+m_slippage)))[2]
        end
        t3=zeros(length(range))

        calib_mass_formula(m,p)=p[1]*m.^p[3] .+ p[2] ## TODO> try: p[1]*(m+p[3]).^0.5 .+ p[2]

        p0=[1.0,1.0,1.0]
        p = Progress(length(range); enabled = progress)
        for i in 1:length(range)
            # Find corresponding optimal timebin
            t1[i]=t1_lower+findmax(raw.data[t1_lower:t1_upper,i+range[1]-1])[2]-1
            t2[i]=t2_lower+findmax(raw.data[t2_lower:t2_upper,i+range[1]-1])[2]-1
            t3[i]=t3_lower+findmax(raw.data[t3_lower:t3_upper,i+range[1]-1])[2]-1
            # Fit a,b,c
            fit=curve_fit(calib_mass_formula, [m1,m2,m3], [t1[i],t2[i],t3[i]], p0)
            a[i]=fit.param[1]
            b[i]=fit.param[2]
            c[i]=fit.param[3]
            next!(p)
        end

        if save
            # create database if not exist
            !haskey(raw.File,"PROCESSED_MassSpec") ? create_group(raw.File, "PROCESSED_MassSpec") : nothing
            !haskey(raw.File["PROCESSED_MassSpec"],"MassCalibration") ? create_group(raw.File["PROCESSED_MassSpec"], "MassCalibration") : nothing
            !haskey(raw.File["PROCESSED_MassSpec/MassCalibration"],"CalibrationParameters") ? create_dataset(raw.File["PROCESSED_MassSpec/MassCalibration"], "CalibrationParameters", Float64, (3,raw.numberCycles)) : nothing

            raw.File["PROCESSED_MassSpec/MassCalibration/CalibrationParameters"][1,range]=a
            raw.File["PROCESSED_MassSpec/MassCalibration/CalibrationParameters"][2,range]=b
            raw.File["PROCESSED_MassSpec/MassCalibration/CalibrationParameters"][3,range]=c

            if range==1:raw.numberCycles
                !haskey(raw.File["PROCESSED_MassSpec/MassCalibration"],"SettingsData") ? create_dataset(raw.File["PROCESSED_MassSpec/MassCalibration"], "SettingsData", Float64, (3,2)) : nothing

                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][1,1]=m1
                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][2,1]=m2
                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][3,1]=m3
                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][1,2]=mean(t1)
                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][2,2]=mean(t2)
                raw.File["PROCESSED_MassSpec/MassCalibration/SettingsData"][3,2]=mean(t3)
            end
        end

    else
        @error "Check the number of masses used for calibration, must be 2 or 3."
    end

    if save && range==1:raw.numberCycles # save averaged mass spectrum
        raw.averageMassAxis[:]=((collect(1:raw.cycleLength) .- mean(b)) / mean(a)) .^ (1/mean(c))
    end

    if !save
        return a,b,c
    end

end
