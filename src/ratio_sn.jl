function ratio_sn(f::TraceData,mass::String,lim1::Tuple,lim2::Tuple)

    if mass[1]=='m'
        config1=f.data[f.masses[mass],:][lim1[1]:lim1[2]]
        config2=f.data[f.masses[mass],:][lim2[1]:lim2[2]]

        n_config1=f.data[f.masses[mass],:][lim1[1]:lim1[2]]./f.correction[lim1[1]:lim1[2]]
        n_config2=f.data[f.masses[mass],:][lim2[1]:lim2[2]]./f.correction[lim1[1]:lim1[2]]
    else
        mass=parse(Float64, mass)
        config1=f.data[f.peaks[mass],:][lim1[1]:lim1[2]]
        config2=f.data[f.peaks[mass],:][lim2[1]:lim2[2]]

        n_config1=f.data[f.peaks[mass],:][lim1[1]:lim1[2]]./f.correction[lim1[1]:lim1[2]]
        n_config2=f.data[f.peaks[mass],:][lim2[1]:lim2[2]]./f.correction[lim1[1]:lim1[2]]
    end

    ## On regarde le Delta_Signal/STD
    println("Ratio signal noise (normalized):")
    println((mean(config1)-mean(config2))/(0.5*(std(config1)+std(config2))), "(",(mean(n_config1)-mean(n_config2))/(0.5*(std(n_config1)+std(n_config2))),")")
end
