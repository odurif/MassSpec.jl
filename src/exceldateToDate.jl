function exceldatetodate(exceldate::Real)
    t,d = modf(exceldate)
    return Dates.DateTime(1899, 12, 30) + Dates.Day(d) + Dates.Millisecond((floor(t * 86400000)))
end

"""
    exceldatetodate(exceldate::Matrix; time::Symbol=:standard)

# Argument
- `exceldate::Matrix`

# Optional argument
- `time::Symbol`: `:standard`, `:absolute` or `:all`
"""
function exceldatetodate(exceldate::Matrix; time::Symbol=:standard)

    date=[exceldatetodate(t) for t in exceldate[1,:]]

    if time==:absolute
        date=map((x) -> convert(DateTime,x),date.-date[1])
        #return [Dates.format(t, "HH:MM") for t in date]
        return [Dates.Time(t) for t in date]
    elseif time==:all
        return date
    else
        #return [Dates.format(t, "HH:MM") for t in date]
        return [Dates.Time(t) for t in date]
    end

end

