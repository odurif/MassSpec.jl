"""
    mutable struct Params <: Reactor

Temperature (in Celsius), pressure (in Pa) and density (in /cm3) within the flow tube reactor.

# Members
- `temperature::Float64=20`
- `pressure::Float64=10220`
- `density::Float64~2.5e19`: corresponding density in /cm3, assuming an ideal gas
"""
@kwdef mutable struct Params <: Reactor
    consts=Constants()
    temperature::Float64 = 20 ## T in Celsius
    pressure::Float64 = 101220 ## P in Pa
    density::Float64 =  pressure/((consts.absZero+temperature)*consts.kb)/1e6 ## /cm3 ## TODO> find a way to recompute if temperature or pressure change...
end

## TODO: calculate the concentration into the Flows struct itself ?!
"""
    struct Flows <: Reactor

Flows in the reactor; flows must be in sccm

# Members
- `compound::Union{String,Symbol}`
- `air::Float64`
- `injection::Float64`
- `dilution::Float64`
- `bubbler::Float64`
"""
@with_kw mutable struct Flows <: FlowTube ## todo > ajouter les concentrations de NO et du precurseur !!
    compound::Union{String,Symbol,Nothing}=nothing
    air::Float64=0
    injection::Float64=0
    dilution::Float64=0
    bubbler::Float64=0
    total::Float64=air+injection
    NO::Float64=0
    NOdilution::Float64=0
    NOinjection::Float64=0
    NO_bottle_concentration::Float64=95.4
    params::Params=Params()
end

"""
    concentration(; air=error(), injection=error(), dilution=error(), bubbler=error(), compound=error(), temperature=nothing, vaporPressure=nothing)

Return the concentration in ppb.
"""
function concentration(; air=error(), injection=error(), dilution=error(), bubbler=error(), compound=error(), T=nothing, Vp=nothing)
    return concentration(Flows(compound=compound,air=air,injection=injection,dilution=dilution,bubbler=bubbler); T=T, Vp=Vp)
end

## TODO: to be improve with 2 dilution stages + better code
"""
    concentration(flows::Flows; T=nothing, Vp=nothing)

Return the concentration in ppb.

# Optional arguments
- `T`: temperature, must be in Kelvin
- `Vp`: vapor pressure, must be in Pascal

# Examples
```julia-repl
julia> flows=Flows(air=2000,injection=10,dilution=600, bubbler=5, compound=:CH3I)
Flows
  compound: Symbol CH3I
  air: Float64 2000.0
  injection: Float64 10.0
  dilution: Float64 600.0
  bulk: Float64 5.0
  total: Float64 2010.0
  NO: Float64 0.0
  NOdilution: Float64 0.0
  NOinjection: Float64 0.0
  NO_bottle_concentration: Float64 95.4
  params: MassSpec.Params
julia> concentration(flows)
Dict{Any, Any} with 4 entries:
  :temperature => 20.0
  :pressure    => 101220.0
  :CH3I        => 17913.3
  :ρTot        => 2.50206e19
```
"""
function concentration(flows::Flows; T=nothing, Vp=nothing)

    if typeof(T)==Nothing
        T=flows.params.temperature
    end

    if typeof(Vp)==Nothing
        Vp=vaporPressure(flows.compound)
    end

    P1=Vp*flows.bubbler/(flows.bubbler+flows.dilution) ## after dilution 1
    P2=P1*flows.injection/(flows.injection+flows.air) ## after in the flow tube
    air_concentration=P2/(T+flows.params.consts.absZero*flows.params.consts.kb)/1e6 ## P2 to concentration in /cm3

    c=Dict()
    c[flows.compound]=air_concentration/flows.params.density*1e9 ## concentration in ppb
    c[:temperature_K]=T ## temperature in Kelvin
    c[:temperature_C]=T+flows.params.consts.absZero ## temperature in Celcius
    c[:pressure]=flows.params.pressure ## pressure in Pascal
    c[:ρTot]=flows.params.density # total density /cm3

    if flows.NOinjection != 0 && flows.NO != 0
        c[:NO] = flows.NOinjection/(flows.total+flows.NOinjection)*flows.NO/(flows.NO+flows.NOdilution)*flows.NO_bottle_concentration*1e3 ## in ppb
    end

    return c

end

## TODO: take from text list or directly from NIST
"""
    vaporPressure(compound::Union{String,Symbol}; unit=:Pa, temperature=nothing)

Returns the vapor pressure in Pascal.

# Examples
```julia-repl
julia> vaporPressure(:C7H7Br, unit=:torr)
20.612230934255955
```
"""
function vaporPressure(compound::Union{String,Symbol}; unit=:Pa, temperature=nothing)

    consts=Constants()
    params=Params()

    if typeof(temperature)==Real
        params.temperature=temperature
    end

    if compound in (:iodomethane,:Iodomethane,"iodomethane","Iodomethane","CH3I", :CH3I)
        a,b,c=4.1554,1177.78,32.058 # Antoine Equation parameters, ref: https://webbook.nist.gov/cgi/cbook.cgi?ID=C74884&Mask=4&Type=ANTOINE&Plot=on
    elseif compound in (:iodoethane,:Iodoethane,"iodoethane","Iodoethane","C2H5I", :C2H5I)
        a,b,c,=4.08511,1247.135,39.612 # Antoine Equation parameters, ref: https://webbook.nist.gov/cgi/cbook.cgi?ID=C75036&Mask=4&Type=ANTOINE&Plot=on
    elseif compound in (:iodopropane,:Iodopropane,"iodopropane","Iodopropane","C3H7I", :C3H7I)
        a,b,c=4.1416,1340.448,38.785 # Antoine Equation parameters, ref:https://webbook.nist.gov/cgi/cbook.cgi?ID=C75309&Units=SI&Mask=4&Type=ANTOINE&Plot=on
    elseif compound in (:dmb, :DMB, "2,3DM2B", "DMB", "dmb", "C6H12")
         a,b,c=3.70468,1021.564,70.242 # Antoine Equation parameters, ref: https://webbook.nist.gov/cgi/cbook.cgi?ID=C563791&Mask=4&Type=ANTOINE&Plot=on
#     elseif compound in (:H2O, :water, "H2O", "water")
#         a,b,c=5.40221,1838.675,31.737 # Antoine Equation parameters, ref: https://webbook.nist.gov/cgi/cbook.cgi?ID=C7732185&Mask=4&Type=ANTOINE&Plot=on
    else
        @error "Unknown compound."
    end

    vp=10^(a - (b / ((params.temperature+consts.absZero) - c))) ## in bar

    return if unit==:Pa || unit==:Pascal
        consts.barInPa*vp ## in Pa
    elseif unit==:bar
        vp
    elseif unit==:torr || unit==:mmHg
        consts.barInTorr*vp ## in Torr
    end

end

"""
    NOppb(df::DataFrame,flows::Flows)::DataFrame

Compute the NO concentration (in ppb) from the NO column of the DataFrame (in sccm) and fixed `flows`.

Return a `DataFrame` with a new column, `NO` (in ppb). Original `NO` columm is renamed `NO (sccm)`
"""
function NOppb(df::DataFrame,flows::Flows)::DataFrame

    # check NO column exist
    if "NO" in names(df)
        df=rename(df, :NO => "NO (sccm)")
    elseif !("NO (sccm)" in names(df))
        @error "NO flow column not found, check your DataFrame."
    end

    # convert NO in sccm to NO in ppb, according to the flows (and changes in NO flows)
    NO_in_ppb=[]
    for NO in df."NO (sccm)"
        flows.NO=NO # change the NO flow
        c=concentration(flows) # recompute concentration
        try # add NO concentration to array
            push!(NO_in_ppb,c[:NO])
        catch
            push!(NO_in_ppb,0)
        end
    end

    # append "NO" (NO in ppb) to df
    return insertcols(df, 1, :NO => NO_in_ppb)

end
