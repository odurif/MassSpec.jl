"Read the averageSignal dataset"
function averageSpectrumSignalDataSet(File::HDF5.File)::HDF5.Dataset
 	if  haskey(File, "PROCESSED") && haskey(File["PROCESSED/"], "AverageSpectrum") && haskey(File["PROCESSED/AverageSpectrum"], "AverageSpectrum") # if processed
		return File["PROCESSED/AverageSpectrum/AverageSpectrum"]
 	elseif haskey(File["SPECdata"], "AverageSpec") # if not processed
 		return File["SPECdata/AverageSpec"]
 	else
 		@error "Average mass spectrum does not exist the raw data."
 	end
end

# TODO: compute this average spectrum with interpolation (see meanSpectrum) ?
# TODO: process into PROCESSED_MassSpec because this path seems no longer use by the viewer!
"Read the averageMassAxis dataset"
function averageMassAxisDataSet(File::HDF5.File, mode::String, averageCalibrationParameters, cycleLength)
 	if  haskey(File, "PROCESSED") && haskey(File["PROCESSED/"], "AverageSpectrum") && haskey(File["PROCESSED/AverageSpectrum"], "AverageMassAxis")
		return File["PROCESSED/AverageSpectrum/AverageMassAxis"]
	elseif mode=="r+" && typeof(averageCalibrationParameters)==HDF5.Dataset
		@info "Compute and save averaged m/z axis"
		averageMassAxis=((collect(1:cycleLength) .- averageCalibrationParameters[2,1]) / averageCalibrationParameters[1,1]) .^ 2
		write_dataset(File, "PROCESSED/AverageSpectrum/AverageMassAxis", averageMassAxis) # save average mass axis
		return File["PROCESSED/AverageSpectrum/AverageMassAxis"]
	else
		@warn "Average m/z axis is missing. Open data in read/write mode to compute it."
		return missing
 	end
end

"Read the averageCalibrationParameters dataset"
function averageCalibrationParametersDataSet(File::HDF5.File)
	if  haskey(File, "PROCESSED") && haskey(File["PROCESSED/"], "AverageSpectrum") && haskey(File["PROCESSED/AverageSpectrum/"], "Masscal")
		if haskey(File["PROCESSED/AverageSpectrum/Masscal/"], "ParametersData") # when data processed
			return File["PROCESSED/AverageSpectrum/Masscal/ParametersData"]
		else
			return missing
		end
	else
		return missing
	end
end

"Return the time when reading the raw data"
function timeDataSet(File::HDF5.File)
	if !haskey(File["SPECdata/"], "Times") # older h5 files
		excelTime=nothing
		realTime=nothing
	elseif size(File["SPECdata/Times"][:,:])[1]==4 # new h5 files
		excelTime=File["SPECdata/Times"][3,:] # excel time
		realTime=File["SPECdata/Times"][4,:] # real time (in sec)
	elseif size(File["SPECdata/Times"][:,:])[1]==3 # old h5 files
		excelTime=File["SPECdata/Times"][2,:] # excel time
		realTime=File["SPECdata/Times"][3,:] # real time (in sec)
	else
		@error "This case should not happen."
	end
# 	time = if haskey(File["SPECdata"], "Times")
# 		unix2datetime.(read(File["SPECdata"]["Times"])[2,:].-2.0828412e9)
# 	end
	return excelTime,realTime
end

"""
	TraceData <: DataFile

Encapsulate the parameters loaded from a PTR-TOF-MS kinetic file (.hdf5)

# Members
- `data::Matrix{Float32}`: Kinetics data
- `masses::Dict{String,Integer}`: Dictionnary for all the masses defined in the peak list
- `time::Vector`: Time coordinate
- `all_signal_each_cycle::Vector{Float64}`: Sum of all the signal of a single mass spectrum
- `correction::Vector{Float64}`: Vector which can be used to normalized the data over the time
- `attributes::Attributes`: Attributes type associated with the kinetics data
- `filePath::String`: name of the file loaded
"""
@with_kw struct TraceData <: DataFile
	data::Matrix{Float32} #::HDF5.Dataset
    masses::Union{Missing,Dict{String,Integer}}
    time::Vector
    all_signal_each_cycle::Vector{Float64}
    correction::Vector{Float64}
    attributes::Attributes
    filePath::String
end

struct PTR_Parameters <: DataFile
	PC_get::Vector{Float64}
end

## TODO: get ride of the 'correction'
"""
	RawData <: DataFile

Encapsulate the data from a PTR-TOF-MS file (.hdf5)

# Members
- `File::HDF5.File`: HDF5 File
- `data::HDF5.Dataset`: Raw data
- `peakTable::HDF5.Dataset`: Peak table
- `calibrationParameters`: Calibration parameters for every spectrum
- `cycleLength::Int64`: Number of bin elements in one mass spectrum
- `numberCycles::Int64`: Number of mass spectra in the data
- `averageMassAxis`: Average mass axis
- `averageSpectrumSignal`: Average spectrum signal axis
- `averageCalibrationParameters`: Calibration parmeters for the averaged spectrum
- `correction::Vector{Float64}`: Vector of length numberCycles which can be used to normalized the data over the time
- `parameters::PTR_Parameters`
- `traces::Union{Missing,MassSpec.TraceData}`:
- `excelTime::Union{Vector{Float64},Nothing}`:
- `realTime::Union{Vector{Float64},Nothing}`:
- `file_mode::String`: open data with 'r' (read) or 'r+' (read/write)
"""
mutable struct RawData <: DataFile ## TODO> revoir la structure, and only mutable in "r+" mode
	File::HDF5.File
    data::HDF5.Dataset
    peakTable#::Union{Missing,HDF5.Dataset} ## TODO add into TraceData?
    calibrationParameters#::Union{Missing,HDF5.Dataset}
    cycleLength::Int64
    numberCycles::Int64
    averageMassAxis#::Union{Missing,HDF5.Dataset}
    averageSpectrumSignal::HDF5.Dataset
    averageCalibrationParameters#::Union{Missing,HDF5.Dataset}
    correction::Vector{Float64}
    parameters::MassSpec.PTR_Parameters
    traces::Union{Missing,MassSpec.TraceData}
    excelTime::Union{Vector{Float64},Nothing}
    realTime::Union{Vector{Float64},Nothing}
    file_mode::AbstractString
end

"""
	LoadRawData(filePath::String; norm::Bool=false, mode::String="r")::RawData

Load raw data file (.h5).

# Argument
- `filePath::String`

# Optional arugment
- `norm::Bool`: normalized desactivated for now because too slow...
- `mode::String="r"`: "r" (read only) or "r+" (read/write)

Return a [`RawData`](@ref)
"""
function LoadRawData(filePath::String; norm::Bool=false, mode::String="r")::RawData

    File=h5open(filePath, mode)
	if !haskey(File, "PROCESSED")
        @warn "No processed data"
	end

    data=File["SPECdata/Intensities"] ## get raw data in a matrix: data[m/z,cycle]

    cycleLength,numberCycles=size(data)
	correction = norm ? maximum(data, dims=2) : ones(cycleLength)

	excelTime,realTime=timeDataSet(File)
    binTime = collect(1:numberCycles)

	averageCalibrationParameters = averageCalibrationParametersDataSet(File)
	averageSpectrumSignal = averageSpectrumSignalDataSet(File)
    averageMassAxis = averageMassAxisDataSet(File, mode, averageCalibrationParameters, cycleLength)
	calibrationParameters=File["PROCESSED/MassCalibration/ParametersData"] ## matrix such as: {Float32,Float32}:cycle


	traces=File["PROCESSED/TraceData/RawData"] ## get traces raw data: traces[peak:cycle]

	# TODO: function for this
	if haskey(File["PROCESSED/TraceData"], "PeakTableData")
		peakTable=File["PROCESSED/TraceData/PeakTableData"]
	elseif mode=="r+"
		#pt=zeros(length(keys(File["PROCESSED/TraceData/PeakTable"])),10)
		#for peak in keys(File["PROCESSED/TraceData/PeakTable"]) ## ## TODO> construct MATRIX based on content in "PROCESSED/TraceData/PeakTable"
			#["label", "center", "low", "high", "ion", "ionic_isotope", "parent", "isotopic_abundance", "k_rate", "multiplier"]
		#end
		#write_dataset(File, "PROCESSED/TraceData/PeakTableData", map(x->"$x", pt)) ## FULL OF ZERO FOR NOW
		if !haskey(File["PROCESSED/TraceData"], "PeakTableInfo")
			write_dataset(File, "PROCESSED/TraceData/PeakTableInfo", ["label","center","low","high","ion","ionic_isotope","parent","isotopic_abundance","k_rate","multiplier"])
		end
		if !haskey(File["PROCESSED/TraceData"], "RawInfo")
			write_dataset(File, "PROCESSED/TraceData/RawInfo", [""]) ## must exist for the PTR-Viewer
		end
		if !haskey(File["PROCESSED/TraceData"], "TraceMasses")
			write_dataset(File, "PROCESSED/TraceData/TraceMasses", [""]) ## must exist for the PTR-Viewer
		end
		peakTable=missing #TODO: create a generic peak table and get ride of the "missing" in the structure
	else
		close(File)
		@error "\"PROCESSED/TraceData/PeakTableData\" not found. Please open in \"r+\" mode to create this dataset."
	end

	masses = !ismissing(peakTable) ? Dict((peak => i for (i,peak) in enumerate(replace.(peakTable[1,:], "half   "=>"m", "half  "=>"m", "half "=>"m")))) : missing

	PC_get=File["AddTraces/PTR-Instrument/Data"][52,:]
	parameters=PTR_Parameters(PC_get)
	traces=TraceData(data=traces[:,:],masses=masses,time=binTime,all_signal_each_cycle=[],correction=correction,attributes=attributes(),filePath=filePath)
	return RawData(File,data,peakTable,calibrationParameters,cycleLength,numberCycles,averageMassAxis,averageSpectrumSignal,averageCalibrationParameters,collect(correction),parameters,traces,excelTime,realTime,mode)

end

#if correction
#	corr=[1/(1/i-1/1000000) for i in data[masses["m19.000"],:]]
#end
"""
	LoadTraceFile(filePath::String; time::Symbol=:standard)::TraceData

Load kinetic file (.txt).

Only plain text supported by now.

## Argument
- `filePath::String`

## Optional arugment
- `time::Symbol`: `:standard`, `:absolute` or `:all`

`peak table.ionipt` file can be loaded for advanced features

Return a [`TraceData`](@ref)
"""
function LoadTraceFile(filePath::String; time::Symbol=:standard)::TraceData

	## load Data
	File=h5open(filePath, "r")
	if !haskey(File,"TimeCycle") || !haskey(File,"Raw") || !haskey(File,"Mass_Names")
		@error "$filePath is not a trace file !"
	end
	data=read(File["Raw"])
	time=exceldatetodate(read(File["TimeCycle"]), time=time)

	masses_names=read(File["Mass_Names"])
	masses=Dict{String,Integer}()

	## TODO: check and simplify
	for (index,id) in enumerate(masses_names)
		if occursin("Peak", id)
			masses[parse(Float64,replace(split(id)[1][2:end],","=>"."))]=index
		elseif split(id)[1][end-3:end] == ",000"
			masses[split(id)[1][1:end-4]]=index
		end
	end

	all_signal_each_cycle=sum(data, dims=1)

	corr=data[masses["m19"],:]

	return TraceData(data=data,masses=masses,time=time,all_signal_each_cycle=all_signal_each_cycle[1,:],correction=corr,attributes=attributes(),filePath=filePath)

end

"""
	LoadSpectrumFile(filePath::String)::DataFrame

Return a DataFrame with two columns: `mass`, `signal`
"""
function LoadSpectrumFile(filePath::String)::DataFrame

	data,header=readdlm(filePath, Float64, header=true);
	return DataFrame(data,vec(header))

end

function LoadSpectra(;folder="spectres", all=true)

    spectra=Dict()
    for spectre in readdir("spectres")
        spectra[spectre[1:end-4]]=Load(folder*"/"*spectre)
    end
    return spectra

end
